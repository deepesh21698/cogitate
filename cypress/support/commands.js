
//custom command for login page
Cypress.Commands.add('login', (username, password) => {
   cy.get("[name='username']").type(username)
   cy.get("[name='password']").type(password)
   cy.get("[class='buttonLogin']").click()
   cy.get('.modalLoader', { timeout: 200000 }).should('not.exist')
})
//custom command for accounthub page
Cypress.Commands.add('generateQuoteIcon', () => {
   cy.get("#sidebar .dropdown", { timeout: 20000 }).last().should('be.visible').click()
   cy.get('.modalLoader', { timeout: 20000 }).should('not.exist')
})
//custom command for risk qualifier page of engs
Cypress.Commands.add('riskQualifierPageEngs', () => {
   cy.get("[type='submit']").click()
   cy.get('.modalLoader', { timeout: 200000 }).should('not.exist')
})
Cypress.Commands.add('riskQualifierPageToga', () => {
   cy.xpath("//*[text()='No']").click({ multiple: true, force: true })
   cy.xpath("//*[text()='Agree']").click({ multiple: true, force: true })
   cy.get('#successBTN').click()
   cy.get('.modalLoader', { timeout: 200000 }).should('not.exist')
})
//custom command for risk bussiness info page of engs
let bussinessInfoMap = new Map()
Cypress.Commands.add('bussinessInfoPageEngs', (agentName, underwriterName, bussinessName, year, address) => {
   //select agent
   cy.get("#agentdetails").select(agentName, { force: true })
   cy.get("#agentdetails").find('option:selected').invoke('text').then((agent) => {
      bussinessInfoMap.set("agent", agent)
   })
   //select underwriter
   cy.get("#underwriterdetails").select(underwriterName, { force: true })
   cy.get("#underwriterdetails").find('option:selected').invoke('text').then((underwriter) => {
      bussinessInfoMap.set("underwriter", underwriter)
   })
   //enter bussiness name
   cy.get("#businessname").type(bussinessName, { force: true }).invoke('val').then((bussinessName) => {
      bussinessInfoMap.set("bussinessName", bussinessName)
   })
   //select year
   cy.get("#yearofincorporation").select(year, { force: true })
   cy.get("#yearofincorporation").find('option:selected').invoke('text').then((year) => {
      bussinessInfoMap.set("year", year)
   })
   //enter address
   cy.get("#react-select-2-input").type(address, { force: true })

   cy.wait(5000)
   cy.get("#react-select-2-input").type('{enter}')
   cy.wait(2000)
   //click on save btn
   cy.get(".col-md-3 .btn").click()
   cy.wait(2000)
   //store saved address
   cy.get(".singleBox table tr").eq(1).invoke('text').then((address) => {
      bussinessInfoMap.set("mailingAddress", address)
   })
   cy.get(".singleBox table tr").eq(2).invoke('text').then((address) => {
      bussinessInfoMap.set("garagingAddress", address)
   })
   //click on submit btn
   cy.get("[type='submit']").click()
   cy.get('.modalLoader', { timeout: 200000 }).should('not.exist')

})
//custom command for vehicle info page of engs
let vehicleInfoMap = new Map()
export let bussinessUse
export let ownership
Cypress.Commands.add('vehicleInfoPageEngs', (vin, year, make, model, sizeClass, distance, exposure, tiv, holderName, pdCarrier) => {
   cy.get('.open-popup-link [alt="Add vehicles Manually"]').click()
   //classification type dropdown
   cy.get("#classificationtype").select("Trucks, Tractors And Trailers")
   cy.get("#classificationtype").find('option:selected').invoke('text').then((classificationType) => {
      vehicleInfoMap.set("classificationType", classificationType)
   })
   //select vehicle type
   cy.get("#vehicletype").select("Dump Truck")
   cy.get("#vehicletype").find('option:selected').invoke('text').then((vehicleType) => {
      vehicleInfoMap.set("vehicleType", vehicleType)
   })
   //vin number
   cy.get("#vehiclevin").type(vin)
   cy.get("#vehiclevin").invoke('val').then((vinNumber) => {
      vehicleInfoMap.set("vinNumber", vinNumber)
   })
   //year dropdown
   cy.get("#year").select(year)
   cy.get("#year").find('option:selected').invoke('text').then((year) => {
      vehicleInfoMap.set("year", year)
   })
   //make dropdown
   cy.get("#make").select(make)
   cy.get("#make").find('option:selected').invoke('text').then((make) => {
      vehicleInfoMap.set("make", make)
   })
   //model name
   cy.get("#model").type(model).invoke('val').then((modelName) => {
      vehicleInfoMap.set("modelName", modelName)
   })
   //size class dropdown
   cy.get("#sizeclass").select(sizeClass)
   cy.get("#sizeclass").find('option:selected').invoke('text').then((sizeClass) => {
      vehicleInfoMap.set("sizeClass", sizeClass)
   })
   //farthest distance dropdown
   cy.get("#farthestdistance").select(distance)
   cy.get("#farthestdistance").find('option:selected').invoke('text').then((distance) => {
      vehicleInfoMap.set("farthestDistance", distance)
   })
   //exposure dropdown
   cy.get("#exposure").select(exposure)
   cy.get("#exposure").find('option:selected').invoke('text').then((exposure) => {
      vehicleInfoMap.set("exposure", exposure)
   })
   //TIV
   cy.get("#vehicleprice").type(tiv).invoke('val').then((tiv) => {
      vehicleInfoMap.set("tiv", tiv)
   })
   //bussiness  use

   cy.get("#Service").then(($bussinessUse) => {

      cy.wait(3000)
      if ($bussinessUse.is(":checked")) {
         bussinessUse = $bussinessUse.prop('value')
      } else {
         bussinessUse = "select the bussiness use"
      }

   })

   cy.get("#Financed").then(($ownership) => {

      cy.wait(3000)
      if ($ownership.is(":checked")) {
         ownership = $ownership.prop('value')
         cy.log("sasdfsdcdssd" + ownership)
      } else {
         ownership = "select the ownership"
      }

   })
   //line holder name
   cy.get("#financer").select(holderName)
   cy.wait(3000)
   cy.get("#financer").find('option:selected').invoke('text').then((holder) => {
      vehicleInfoMap.set("lineHolderName", holder)
   })
   let holder = vehicleInfoMap.get("lineHolderName")
   cy.log(holder)
   if (vehicleInfoMap.get("lineHolderName") === "Other") {
      cy.get("#otherFinancer").type("jwhddhwjkwhjkh")
      //line holder address
      cy.get("#financerAddress").type("svxhgvshx").invoke('val').then((holderAddress) => {
         vehicleInfoMap.set("lineHolderAddress", holderAddress)
      })
   }
   //line holder address
   cy.get("#financerAddress").invoke('val').then((holderAddress) => {
      vehicleInfoMap.set("lineHolderAddress", holderAddress)
   })

   //pd carrier dropdown
   cy.get("#optedCarrier").select(pdCarrier)
   cy.get("#optedCarrier").find('option:selected').invoke('text').then((carrier) => {
      vehicleInfoMap.set("carrier", carrier)
   })
   //save the details
   cy.get("#SubmitOnly").click()
   //continue button
   cy.get('#pageSubmitButton').click()
   //wait for looader to exist
   cy.get('.modalLoader', { timeout: 200000 }).should('not.exist')
   cy.get('#brajydg', { timeout: 4000 }).should('not.exist')
})
let coveragePageMap = new Map()
//custom command for coverage page
Cypress.Commands.add('coverageInfoPageEngs', (deductibleAmount) => {
   cy.intercept({
      url: "https://testengsgql.azurewebsites.net/api/PolicyGraphQL?",
      method: "POST"
   }, (req) => {
      if (req.body.operationName !== 'Mutation') {
         req.alias = 'apiResponse'
         req.continue()
      }

   })
//select deductible value
   cy.get(".inputText").select(deductibleAmount)
   cy.get(".inputText").find('option:selected').invoke('text').then((amount) => {
      coveragePageMap.set("deductibleAmount", amount)
   })
//submit details
   cy.get("[type='submit']").click()
//wait for response
   cy.wait('@apiResponse').then((res)=>{
      cy.log(res.response.body)
      cy.log(res.response.body.data.MultipleRate.Policy[1].TotalPremium.AnnualPremium)
      let annualPremium = res.response.body.data.MultipleRate.Policy[1].TotalPremium.AnnualPremium
        coveragePageMap.set("annualPremium",annualPremium)

   })
   cy.get('.modalLoader', { timeout: 200000 }).should('not.exist')
})


//command for get current date
let year
let formattedDay
let formattedMonth
let currentDate = () => {
   //create object for date
   let currentDate = new Date()
   //get date
   let date = currentDate.getDate()
   //get month
   let month = currentDate.getMonth() + 1
   //get full year
   year = currentDate.getFullYear()
   //formatted date and month
   formattedDay = String(date).padStart(2, '0');
   formattedMonth = String(month).padStart(2, '0');
}

export function getPolicyEffectiveDate() {
   currentDate()
   let formattedDate = `${formattedMonth}/${formattedDay}/${year}`
   return formattedDate
}
export function getPolicyExpirationDate() {
   currentDate()
   let formattedDate = `${formattedMonth}/${formattedDay}/${year + 0o1}`
   return formattedDate
}
export function getRandomNumber(a) {
   let number = Math.floor(Math.random() * a) + 1
   return number
}
export function generateRandomNumber(a) {
   let number = Math.floor(Math.random() * a)
   return number
}
//custom command for write/update in xlsm file

Cypress.Commands.add('modifyExcelFile', (filePath, sheetName, cell, value) => {
   return cy.task('modifyExcelFile', { filePath, sheetName, cell, value });
})


Cypress.Commands.add('readExcel', (filePath, sheetName) => {
   return cy.task('readExcelFile', { filePath, sheetName })
})


//custom command for generate email
export function email() {
   let randomData = "hwqdhwkhdjkwhjkdwjkedkjdiwjiueiwyreytybvzbnvcnxzcbxvcnvncvnzvvbnv321231321vvnavj121v3hj123v1h2v3vh12vamzkdjkhjgh4jhjqkhkj99877j"
   let mail = ""
   for (var i = 0; i < 5; i++) {
      mail += randomData.charAt(Math.floor(Math.random() * randomData.length))
   }
   let email = mail + "@gmail.com"
   return email
}
//-------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------

//custom command for bussiness info page

//custom command for risk qualifier page of toga
Cypress.Commands.add('togaQuestions', () => {
   cy.xpath("//*[text()='No']").click({ multiple: true, force: true })
   cy.xpath("//*[text()='Agree']").click({ multiple: true, force: true })
   cy.get("[type='submit']").click({ force: true })
   cy.get('.modalLoader', { timeout: 200000 }).should('not.exist')
})
//get month in text
export function getCurrentMonthText(currentMonthIndex) {
   const monthNames = [
       'January', 'February', 'March', 'April', 'May', 'June',
       'July', 'August', 'September', 'October', 'November', 'December'
   ];
   const currentMonthText = monthNames[currentMonthIndex];
   return currentMonthText;
}
//bussiness toga map
let bussinesinfoTogaMap = new Map()
Cypress.Commands.add("bussinessinfoDetails", (retail, underwriter, usDot, insured, bussinessName, dba, description, subsidiary, motorCarrier, sms, phoneNumber, address, commodity, hauled, maxValue, avgValue, operationType, majorCity, state, radius, radiusPercentage) => {
   //open retail dropdown
   cy.get('input[id="react-select-2-input"]').first().click({ force: true })
   //retail list
   cy.get(".css-lkh0o5-menu>div>div", { timeout: 10000 }).each(($retail) => {
      if ($retail.text().includes(retail)) {
         cy.wrap($retail).click({ force: true })
      }
   })
   //get retail value
   cy.get('input[id="react-select-2-input"]').first().parent().prev()
      .invoke('text').then((retailAgency) => {
         bussinesinfoTogaMap.set("retailAgency", retailAgency)
      })
   //open agent dropdown
   cy.xpath('//*[@id="react-select-3-placeholder"]/following-sibling::div /input').as("agent").click({ force: true })
   //select agent
   cy.get(".css-lkh0o5-menu>div>div", { timeout: 10000 }).each(($ele) => {
      if ($ele.text().includes("Allegiance Insurance Services, LLC (214426)")) {
         cy.wrap($ele).click({ force: true })
      }
   })
   //get agent
   cy.get("@agent").parent().prev().invoke('text').then((agent) => {
      cy.log(agent)
      bussinesinfoTogaMap.set("agent", agent)
   })
   //open underwriter dropdown
   cy.xpath('//*[@id="react-select-4-placeholder"]/following-sibling::div /input').as("underwriter").click({ force: true })
   //select underwriter
   cy.get(".css-lkh0o5-menu>div>div", { timeout: 10000 }).each(($underwriter) => {
      if ($underwriter.text().includes(underwriter)) {
         cy.wrap($underwriter).click({ force: true })
      }
   })
   //get underwriter
   cy.get("@underwriter").parent().prev().invoke('text').then((underwriter) => {
      cy.log(underwriter)
      bussinesinfoTogaMap.set("underwriter", underwriter)
   })
   //enter us dot value
   cy.get("#usdot").type(usDot, { force: true }).invoke('text').then((usdot) => {
      cy.log(usDot)
      bussinesinfoTogaMap.set("usdot", usdot)
   })
   //open insured type dropown
   cy.xpath('//*[@id="react-select-5-placeholder"]/following-sibling::div /input').as("insuredType").click({ force: true })
   //select insured type
   cy.xpath("//*[@id='react-select-5-listbox']/div/div", { timeout: 10000 }).each(($ele) => {
      if ($ele.text().includes(insured)) {

         cy.wrap($ele).click({ force: true })
         cy.wait(3000)
      }
   })
   cy.get("@insuredType").parent().prev().invoke('text').then((type) => {
      cy.log(type)
      bussinesinfoTogaMap.set("insuredType", type)
      if (bussinesinfoTogaMap.get("insuredType") === 'Individual') {
         cy.get("#firstname").type("Jee", { force: true })
         cy.get("#middlename").type("sam", { force: true })
         cy.get("#lastname").type("Company", { force: true })

      } else {
         //enter bussines name field
         cy.get("#businessName").type(bussinessName, { force: true })
      }
   })

   //enter dba
   cy.get("#dbaName").type(dba, { force: true }).invoke('text').then((dba) => {
      cy.log(dba)
      bussinesinfoTogaMap.set("dba", dba)
   })
   //enter bussiness description
   cy.get("#businessDescription").type(description, { force: true })
   //open subsdiary
   cy.xpath('//*[@id="react-select-6-input"]').as("subsidiary").click({ force: true })
   //select subsidiary
   cy.xpath('//*[@id="react-select-6-listbox"]/div/div').each(($subsidiary) => {
      if ($subsidiary.text().includes(subsidiary)) {
         cy.wrap($subsidiary).click({ force: true })
      }
   })
   //get subsidiary
   cy.get("@subsidiary").parent().prev().invoke('text').then((subsidiary) => {
      cy.log(subsidiary)
      bussinesinfoTogaMap.set("subsidiary", subsidiary)
   })
   //select effective date of coverage
   let date = new Date()
   let month = date.getMonth() + 1
   let day = date.getDate()
   cy.get('#effectivedatecoverage input').eq(1).then(($date) => {
       //open calender
       cy.wrap($date).click({ force: true })
       cy.wait(2000)
       cy.get('.react-calendar__navigation__label').first().click({ force: true })
       cy.wait(2000)
       cy.contains(getCurrentMonthText(month)).click({ force: true })
       cy.wait(2000)
       cy.contains(day).click({ force: true })
    
   })
      // select current info policy button
   cy.get('[name="btnGrp_currentInsurancePolicyInfo"] .btn-buttNo').click({ force: true })
   //selec mid term no btn
   cy.get('[name="btnGrp_isinsuredlookmovemidterm"] > .btn-buttNo').click({ force: true })
   //select policy declined no buton
   cy.get('[name="btnGrp_policydeclinthreeyears"]  .btn-buttNo').click({ force: true })
   //enter motor carrier
   cy.get("#motorcarrier").type(motorCarrier, { force: true })
   //enter bussiness started date
   cy.wait(1000)
   cy.get("#incorporationDate").find("input").eq(1)
      .type("003", { force: true }).invoke('text').then((incorporationMonth) => {
         cy.log(incorporationMonth)
         bussinesinfoTogaMap.set("incorporationMonth", incorporationMonth)
      })
      cy.wait(1000)
   cy.get("#incorporationDate").find("input").eq(2)
      .type("2010", { force: true }).invoke('text').then((incorporationYear) => {
         cy.log(incorporationYear)
         bussinesinfoTogaMap.set("incorporationYear", incorporationYear)
      })
   //change in ownership no button
   cy.get('[name="btnGrp_isPastOwnershipChanged"] .btn-buttNo').click({ force: true })
   //select sms alert value
   cy.xpath('//*[@id="react-select-7-input"]').click({ force: true })
   //select sms value
   cy.xpath('//*[@id="react-select-7-listbox"] /div/div').each(($sms) => {
      if ($sms.text().includes(sms)) {
         cy.wrap($sms).click({ force: true })
      }
   })
   //enter first name 
   cy.get('.formBoxHgt:nth-child(3) #firstname').type("Sam", { force: true })
      .invoke('text').then((contactFirstName) => {
         cy.log(contactFirstName)
         bussinesinfoTogaMap.set("contactFirstName", contactFirstName)
      })
   //enter middle name
   cy.get('.formBoxHgt:nth-child(3) #middlename').type("Kumar", { force: true })
      .invoke('text').then((contactMiddleName) => {
         cy.log(contactMiddleName)
         bussinesinfoTogaMap.set("contactMiddleName", contactMiddleName)
      })
   //enter last name
   cy.get('.formBoxHgt:nth-child(3) #lastname').type("dar", { force: true })
      .invoke('text').then((contactLastName) => {
         cy.log(contactLastName)
         bussinesinfoTogaMap.set("contactLastName", contactLastName)
      })
   //enter email
   cy.get("#email").type("barsiwaldeepesh@bugraptors.com", { force: true }).invoke('text').then((contactMail) => {
      cy.log(contactMail)
      bussinesinfoTogaMap.set("contactMail", contactMail)
   })
   //enter phone number
   cy.get("#mobilephone").type(phoneNumber, { force: true })
      .invoke('text').then((contactPhone) => {
         cy.log(contactPhone)
         bussinesinfoTogaMap.set("contactPhone", contactPhone)
      })
   //enter address
   cy.get('[name="locationTable"] input').type(address, { force: true })
   cy.wait(2000)
   //press enter
   cy.get('#react-select-2-option-0', { timeout: 4000 }).should('be.visible').click({ force: true })
   //check mailing address checkbox
   cy.xpath('//label[@for="Mailing Address"] /preceding-sibling :: input').invoke('css', 'opacity', '1').check({ force: true })
   //check garaging address checkbox
   cy.xpath('//label[@for="Garaging Address"] /preceding-sibling :: input').invoke('css', 'opacity', '1').check({ force: true })
   cy.wait(3000)
   //save the address
   cy.get(".col-md-3 .btn").click({ force: true })
   //get address
   cy.get('.react-bootstrap-table tbody tr').eq(1).invoke('text').then((address) => {
      bussinesinfoTogaMap.set("garagingAddress", address)
   })

   //open commodity dropdown
   cy.xpath("//*[@id='react-select-10-input']").click({ force: true })
   //select commodity
   cy.xpath("//*[@id='react-select-10-listbox'] /div/div").each(($commodity) => {
      if ($commodity.text().includes(commodity)) {
         cy.wrap($commodity).click({ force: true })
      }
   })
   //enter hauled field
   cy.get('input[id="PercentageHauled.0"]').type(hauled, { force: true })
   //enter maximum value
   cy.get('input[id="MaximumValue.0"]').type(maxValue, { force: true })
   //enter average value
   cy.get('input[id="Averagevalue.0"]').type(avgValue, { force: true })
   //travelling out no btn
   cy.get('[name="btn_travellingOutofState1"]').click({ force: true })
   //open operation dropdown
   cy.get('input[id="react-select-8-input"]').click({ force: true })
   //select operation
   cy.get('[id="react-select-8-listbox"]>div>div').each(($operation) => {
      if ($operation.text().includes(operationType)) {
         cy.wrap($operation).click({ force: true })
      }
   })
   //enter major cities
   cy.get('[id="citiestravelled"]').type(majorCity, { force: true })
   //open state dropdown
   cy.get('input[id="react-select-9-input"]').click({ force: true })
   //select state
   cy.get('[id="react-select-9-group-0-heading"]+div div').each(($sates) => {
      if ($sates.text().includes(state)) {
         cy.wrap($sates).click({ force: true })
      }
   })
   //open radius operation dropdown
   cy.get('input[id="react-select-11-input"]').click({ force: true })
   cy.get('[id="react-select-11-listbox"]>div div').each(($radius) => {
      if ($radius.text().includes(radius)) {
         cy.wrap($radius).click({ force: true })
      }
   })
   //enter percentage
   cy.get('[id="percentofoperation.0"]').type(radiusPercentage, { force: true })
   //click on save btn
   cy.get("[type='submit']").click({ force: true })
   cy.get('.modalLoader', { timeout: 200000 }).should('not.exist')
})



//custom command for vehicle info page

Cypress.Commands.add('vehicleInfo', (vin, type, year, make, model, stated, annual, ownership, trailer, vice, unit) => {
   cy.get("#vehiclevin").type(vin)
   //click yes btn 
   cy.get('[name="btn_overridevin0"]').click({ force: true })
   //open vehicle type
   cy.get('[class=" css-1xc3v61-indicatorContainer"]').eq(0).click({ force: true })
   //select value
   cy.get('.css-qr46ko>div').each(($type) => {
      if ($type.text().includes(type)) {
         cy.wrap($type).click({ force: true })
      }
   })
   //select year
   cy.get(':nth-child(5) > .custSelect > .css-b62m3t-container > .css-1im77uy-control > .css-hlgwow > .css-19bb58m')
      .click({ force: true })
   //select value
   cy.get('.css-qr46ko>div').each(($year) => {
      if ($year.text().includes(year)) {
         cy.wrap($year).click({ force: true })
      }
   })
   //open make dropdown
   cy.get('[class=" css-1xc3v61-indicatorContainer"]').eq(1).click({ force: true })
   //select value
   cy.get('.css-qr46ko>div').each(($make) => {
      if ($make.text().includes(make)) {
         cy.wrap($make).click({ force: true })
      }
   })
   //enter model
   cy.get("#model").type(model)
   cy.get('#statedvalue').type(stated)
   cy.get('#annualmileage').type(annual)

   //vehicle ownership
   cy.get('[class=" css-1xc3v61-indicatorContainer"]').eq(4).click({ force: true })
   cy.wait(3000)
   cy.get('.css-qr46ko >div').each(($ele) => {
      if ($ele.text().includes(ownership)) {
         cy.wrap($ele).click({ force: true })
      }
   })
   // //trailers type
   cy.get(':nth-child(12) > .custSelect > .css-b62m3t-container > .css-1im77uy-control > .css-hlgwow > .css-19bb58m').click({ force: true })
   cy.get('.css-qr46ko >div').each(($ele) => {
      if ($ele.text().includes(trailer)) {
         cy.wrap($ele).click({ force: true })
      }
   })
   //select no logging
   cy.get('[name="btnGrp_electroniclogging"] .btn-buttNo').click({ force: true })
   //node vice 
   cy.get('#nodevice').type(vice)
   //camera unit
   cy.get('[class=" css-1xc3v61-indicatorContainer"]').last().click({ force: true })
   cy.get('.css-qr46ko >div').each(($ele) => {
      if ($ele.text().includes(unit)) {
         cy.wrap($ele).click({ force: true })
      }
   })
   //submit
   cy.get('#submitBTN').click({ force: true })
   cy.wait(3000)
   //click on next btn
   cy.get('#btnnext', { timeout: 30000 }).should('be.visible').click({ force: true })
   cy.get('.modalLoader', { timeout: 200000 }).should('not.exist')
})
//custom command for driver info
let driverinfoTogaMap = new Map()
Cypress.Commands.add("driverinfoPage", (firstName, lastName, driverType, licence, state, mobileNumber, year, violation) => {
   //enter first  name
   cy.get('[id="driverfirstname"]').type(firstName)
   //enter last name
   cy.get('[id="driverlastname"]').type(lastName)
   //enter dob
   cy.get('[id="dob"] input').eq(1).type("05")
   cy.wait(2000)
   cy.get('[id="dob"] input').eq(2).type("05")
   cy.wait(2000)
   cy.get('[id="dob"] input').eq(3).type("1970")

   //driver type
   cy.get('[class=" css-1xc3v61-indicatorContainer"]').eq(0).click({ force: true })
   cy.get('.css-qr46ko >div').each(($ele) => {
      if ($ele.text().includes(driverType)) {
         cy.wrap($ele).click({ force: true })
      }
   })
   //enter license number
   cy.get('[id="commerciallicensenumber"]').type(licence).invoke('val').then((licence)=>{
      driverinfoTogaMap.set("license",licence)
   })
   //open commercial license state dropdown
   cy.get('[class=" css-1xc3v61-indicatorContainer"]').eq(1).click({ force: true })
   cy.get('.css-qr46ko >div>div>div').each(($ele) => {
      if ($ele.text().includes(state)) {
         cy.wrap($ele).click({ force: true })
      }
   })
   //enter email
   cy.get('[id="email"]').type("barsiwaldeepesh@bugraptors.com")
   //enter mobile number
   cy.get('[id="mobilephone"]').type(mobileNumber)
   //enter date of hire
   cy.get('[id="dateofhire"] input').eq(1).type("05")
   cy.get('[id="dateofhire"] input').eq(2).type("05")
   cy.get('[id="dateofhire"] input').eq(3).type("2010")
   //open year dropdown
   cy.get('[class=" css-1xc3v61-indicatorContainer"]').eq(2).click({ force: true })
   cy.get('.css-qr46ko >div').each(($ele) => {
      if ($ele.text().includes(year)) {
         cy.wrap($ele).click({ force: true })
      }
   })
   //violation dropdown
   cy.get(':nth-child(13) > .custSelect > .css-b62m3t-container > .css-1im77uy-control > .css-hlgwow > .css-19bb58m')
      .click({ force: true })
   cy.get('.css-qr46ko >div').each(($ele) => {
      if ($ele.text().includes(violation)) {
         cy.wrap($ele).click({ force: true })
      }
   })
   //accident dropdown
   cy.get('[class=" css-1xc3v61-indicatorContainer"]').eq(3).click({ force: true })
   cy.get('.css-qr46ko >div').each(($ele) => {
      if ($ele.text().includes(violation)) {
         cy.wrap($ele).click({ force: true })
      }
   })
   //upload mvr
   cy.get('[id="uploadmvr"]').selectFile('cypress/fixtures/sample.pdf')
   cy.get('[id="uploadmvr"]').invoke('val').then((file)=>{
      cy.log(file)
     const fileName = file.split('\\')[2]
     driverinfoTogaMap.set("mvrFileName",fileName)
     cy.log(fileName)
   })
   ///get uploaded file name
   //*[@id="uploadmvr"]  //parent ::div //following-sibling ::div //label[2]
   cy.wait(3000)
   //submit the details
   cy.get('[id="submitBTN"]').click({ force: true })
   //click on next btn
   cy.get('#btnnext').click({ force: true })
   cy.get('.modalLoader', { timeout: 200000 }).should('not.exist')

})

//custom commands for operation details
let operationDetailTogaMap = new Map()
Cypress.Commands.add("operationdetailsPage", (lob, lossYear, loss, insuranceCompany, premium, reserve, paid) => {
   //selet lob
   cy.get('[class=" css-1xc3v61-indicatorContainer"]').eq(0).click({ force: true })
   cy.get('.css-qr46ko >div').each(($ele) => {
      if ($ele.text().includes(lob)) {
         cy.wrap($ele).click({ force: true })
      }
   })
   //select year of loss
   cy.get('[class=" css-1xc3v61-indicatorContainer"]').last().click({ force: true })
   cy.get('.css-qr46ko >div').each(($ele) => {
      if ($ele.text().includes(lossYear)) {
         cy.wrap($ele).click({ force: true })
      }
   })
   //enter losses
   cy.get('[id="numberoflosses.0"]').type(loss, { force: true })
   //enter insurance company
   cy.get('[id="carriers.0"]').type(insuranceCompany, { force: true })
   // enter annual premium
   cy.get('[id="annualPremiumLabel.0"]').type(premium, { force: true })
   //enter reserve
   cy.get('[id="reserves.0"]').type(reserve, { force: true })
   //enter paid
   cy.get('[id="paid.0"]').type(paid, { force: true })
   //select no claim pending
   cy.get('[name="btnGrp_isClaimPending.0"] .btn-buttNo').click({ force: true })
   //upload loss run file
   cy.get('[id="uploadLossRuns.0"]').selectFile("cypress/fixtures/toga.pdf", { force: true })
   cy.get('[id="uploadLossRuns.0"]').invoke('val').then((file)=>{
      cy.log(file)
     const fileName = file.split('\\')[2]
     operationDetailTogaMap.set("lossFileName",fileName)
     cy.log(fileName)
   })
   //select no btn
   cy.get('.riskQual .btn-buttNo').click({ force: true, multiple: true })
   //click on next btn
   cy.get('#btnnext').click({ force: true })
   cy.get('.modalLoader', { timeout: 200000 }).should('not.exist')

})
//custom command for coverage info page
Cypress.Commands.add("coverageinfoPage", (tiv, towingAmount, nonowned, deductible, limit, debris, premiumCharge, flatbedDeductible, truckLimit, benefitLimit, medicalPayment) => {
   //select no waiver subrogation
   cy.get('[name="btnGrp_wostoggle"] .btn-buttNo').click({ force: true })
   //select pip no btn
   cy.get('[id="radio-piptoggle"].btn-buttNo').click({ force: true })
   //select um no btn
   cy.get('[name="btnGrp_umtoggle"] .btn-buttNo').click({ force: true })
   //select uim no btn
   cy.get('[name="btnGrp_uimtoggle"] .btn-buttNo').click({ force: true })
   //select uiia no btn
   cy.get('[name="btnGrp_uiiatoggle"] .btn-buttNo').click({ force: true })
   //select hired auto no btn
   cy.get('[name="btnGrp_hiredautotoggle"] .btn-buttNo').click({ force: true })
   //select tria no btn
   cy.get('[name="btnGrp_liabilitytriatoggle"] .btn-buttNo').click({ force: true })
   //enter tiv
   cy.get('[id="comprehensive"]').type(tiv, { force: true })
   //open towing labour dropdown
   cy.get('[class=" css-1xc3v61-indicatorContainer"]').eq(0).click({ force: true })
   cy.get('.css-qr46ko >div').each(($ele) => {
      if ($ele.text().includes(towingAmount)) {
         cy.wrap($ele).click({ force: true })
      }
   })
   //selct no uiia pysical damage
   cy.get('[name="btnGrp_uiiaphysicaldamge"] .btn-buttNo').click({ force: true })
   //non owned endorsemnet
   cy.get('[id="nonownedtrailer"]').type(nonowned, { force: true })
   //select deductible
   cy.get('[class=" css-1xc3v61-indicatorContainer"]').eq(1).click({ force: true })
   cy.get('.css-qr46ko >div').each(($ele) => {
      if ($ele.text().includes(deductible)) {
         cy.wrap($ele).click({ force: true })
      }
   })
   cy.wait(3000)
   //select tria physical damage value
   cy.get('[name="btnGrp_triaphysicaldamagetoggle"] .btn-buttNo').click({ force: true })
   //select limit
   cy.get('[class=" css-1xc3v61-indicatorContainer"]').eq(2).click({ force: true })
   cy.get('.css-qr46ko >div').each(($ele) => {
      if ($ele.text().includes(limit)) {
         cy.wrap($ele).click({ force: true })
      }
   })
   //select contigent coverage
   cy.get('[name="btnGrp_transitcoverage"] .btn-buttNo').click({ force: true })
   //select fright coverage no btn
   cy.get('[name="btnGrp_freightcoverage"] .btn-buttNo').click({ force: true })
   //select ltl coverage no btn
   cy.get('[name="btnGrp_ltlCoverage"] .btn-buttNo').click({ force: true })
   //select debris removal
   cy.get('[class=" css-1xc3v61-indicatorContainer"]').eq(3).click({ force: true })
   cy.get('.css-qr46ko >div').each(($ele) => {
      if ($ele.text().includes(debris)) {
         cy.wrap($ele).click({ force: true })
      }
   })
   //full premium charge
   cy.get('[class=" css-1xc3v61-indicatorContainer"]').eq(4).click({ force: true })
   cy.get('.css-qr46ko >div').each(($ele) => {
      if ($ele.text().includes(premiumCharge)) {
         cy.wrap($ele).click({ force: true })
      }
   })

   //select uiia no endorsement
   cy.get('[name="btnGrp_uiiaendorsement"] .btn-buttNo').click({ force: true })
   //select unattended truck coverage no
   cy.get('[name="btnGrp_unattendedtruck"] .btn-buttNo').click({ force: true })
   //select riggers no
   cy.get('[name="btnGrp_riggerscoverage"] .btn-buttNo').click({ force: true })
   //open flat bed deductible dropdown
   cy.get('[class=" css-1xc3v61-indicatorContainer"]').eq(5).click({ force: true })
   cy.get('.css-qr46ko >div').each(($ele) => {
      if ($ele.text().includes(flatbedDeductible)) {
         cy.wrap($ele).click({ force: true })
      }
   })

   //select motor truck tria no btn
   cy.get('[name="btnGrp_motortria"] .btn-buttNo').click({ force: true })

   cy.wait(3000)
   //select truck limit
   cy.get(':nth-child(4) > .boxWrapper > .row > :nth-child(1) > .input-group > .custSelect > .css-b62m3t-container > .css-1im77uy-control > .css-hlgwow > .css-19bb58m').click({ force: true })
   cy.get('.css-qr46ko >div').each(($ele) => {
      if ($ele.text().includes(truckLimit)) {
         cy.wrap($ele).click({ force: true })
      }
   })
   //select employee benefit limit
   cy.get('[class=" css-1xc3v61-indicatorContainer"]').eq(7).click({ force: true })
   cy.get('.css-qr46ko >div').each(($ele) => {
      if ($ele.text().includes(benefitLimit)) {
         cy.wrap($ele).click({ force: true })
      }
   })
   //select medical payment
   cy.get('[class=" css-1xc3v61-indicatorContainer"]').eq(8).click({ force: true })
   cy.get('.css-qr46ko >div').each(($ele) => {
      if ($ele.text().includes(medicalPayment)) {
         cy.wrap($ele).click({ force: true })
      }
   })
   //select primary non contributory
   cy.get('[name="btnGrp_primarynoncontributory"] .btn-buttNo').click({ force: true })
   //select right of recovery no btn
   cy.get('[name="btnGrp_rightsofrecovery"] .btn-buttNo').click({ force: true })
   //select truck tria  no btn
   cy.get('[name="btnGrp_generatliablitytria"] .btn-buttNo').click({ force: true })
   //select trailer interchange no btn
   cy.get('[name="btnGrp_trailerrequired"] .btn-buttNo').click({ force: true })

   //click on next btn
   cy.get('#btnnext').click({ force: true })
   cy.get('.modalLoader', { timeout: 200000 }).should('not.exist')

})
//custom command for quote indication
let quoteIndicationTogaMap = new Map()
Cypress.Commands.add("quoteIndicationPage", () => {
   //get premium
   cy.get('[class="row premSect"] .premHead p').then(($premium) => {
      const premiumTxt = $premium.text().replace("$", "")
      const premiumNumber = Number(premiumTxt)
      quoteIndicationTogaMap.set("premium", premiumNumber)

   })
   //click on next btn
   cy.get('#btnnext').click({ force: true })
   cy.get('.modalLoader', { timeout: 200000 }).should('not.exist')
})
Cypress.Commands.add("generalLiabilitiesPage",
   (threeYearPowerUnit, threeYearTotalMile, threeYearReceipt, twoYearPowerUnit, twoYearTotalMile, twoYearReceipt, priorYearPowerUnit, priorYearTotalMile, priorYeaReceipt, currentYearPowerUnit, currentYearTotalMile, currentYearReceipt, nxtYearPowerUnit, nxtYearTotalMile, nxtYearReceipt, security, vehicletransist, experience, otherCriteria, boardfireRate) => {
      //enter 3 year power unit
      cy.get("#threeyearspowerunits").type(threeYearPowerUnit, { force: true })
      cy.get('#threeyearstotalmiles').type(threeYearTotalMile, { force: true })
      //enter gross receipt
      cy.get('#threeyearsgrossreciepts').type(threeYearReceipt, { force: true })
      //enter 2 year power unit
      cy.get('#twoyearspowerunits').type(twoYearPowerUnit, { force: true })
      //enter 2 year mile
      cy.get("#twoyearstotalmiles").type(twoYearTotalMile, { force: true })
      //enter 2 year receipt
      cy.get("#twoyearsgrossreciepts").type(twoYearReceipt, { force: true })
      //enter prior year power unit
      cy.get('#prioryearspowerunits').type(priorYearPowerUnit, { force: true })
      //enter prior year total mile
      cy.get('#prioryearstotalmiles').type(priorYearTotalMile, { force: true })
      //enter prior year receipt
      cy.get('#prioryearsgrossreciepts').type(priorYeaReceipt, { force: true })
      //enter current year unit
      cy.get('#currentyearpowerunits').type(currentYearPowerUnit, { force: true })
      //enter current year total mile
      cy.get('#currentyeartotalmiles').type(currentYearTotalMile, { force: true })
      //enter current year receipt
      cy.get('#currentyeargrossreciepts').type(currentYearReceipt, { force: true })
      //enter nxt year power unit
      cy.get('#nextyearpowerunits').type(nxtYearPowerUnit, { force: true })
      //enter nxt year power unit
      cy.get('#nextyeartotalmiles').type(nxtYearTotalMile, { force: true })
      //enter nxt year power unit
      cy.get('#nextyeargrossreciepts').type(nxtYearReceipt, { force: true })
      //select no equipment not listed question
      cy.get('[name="btn_equipmentnotlistedtoggle1"]').click({ force: true })
      //select no hazardous question
      cy.get('[name="btn_hazardouscommoditieslabeltoggle1"]').click({ force: true })
      //select trailer detached no btn
      cy.get('[name="btn_trailerdetachedtoggle1"]').click({ force: true })
      //select trailer detached no btn
      cy.get('[name="btn_unloadedorunattendedtoggle1"]').click({ force: true })
      //select trailer detached no btn
      cy.get('[name="btn_detachedfrompowerunitstoggle1"]').click({ force: true })
      //security vehicle dropdown
      cy.get('[class=" css-1xc3v61-indicatorContainer"]').eq(0).click({ force: true })
      cy.get('.css-qr46ko >div').each(($ele) => {
         if ($ele.text().includes(security)) {
            cy.wrap($ele).click({ force: true })
         }
      })
      //loaded vehicle transist
      cy.get('[class=" css-1xc3v61-indicatorContainer"]').last().invoke('css', 'z-index', '1').click({ force: true })
      cy.get('.css-qr46ko >div').each(($ele) => {
         if ($ele.text().includes(vehicletransist)) {
            cy.wrap($ele).click({ force: true })
         }
      })
      //driving hiring experience
      cy.get('#DrivingHiringExperienceText').type(experience, { force: true })
      //driving hiring other criteria
      cy.get("#DrivingHiringOtherText").type(otherCriteria, { force: true })
      //select truck and equipment question no button
      cy.get('.formBox:nth-child(5) .btn-buttNo').click({ force: true, multiple: true })
      //enter board fire rate
      cy.get('#terminalpremisestext').type(boardfireRate, { force: true })
      //select criminal record questions no button
      cy.get('.formBox:nth-child(6) .btn-buttNo').click({ force: true, multiple: true })
      //select bussiness history questions no button
      cy.get('.formBox:nth-child(7) .btn-buttNo').click({ force: true, multiple: true })
      //select premises facilities question no button
      cy.get('.formBox:nth-child(8) .btn-buttNo').click({ force: true, multiple: true })
      //select employee question no button
      cy.get('.formBox:nth-child(9) .btn-buttNo').click({ force: true, multiple: true })
      cy.get('#btnnext').click({ force: true })
      //select ok btn
      cy.get("#successBTN").click({ force: true })
      //click on next btn
      cy.get('#btnnext').click({ force: true })
      cy.get('.modalLoader', { timeout: 200000 }).should('not.exist')
   })
//custom command for toga
let accounthubTogaMap = new Map()
Cypress.Commands.add("accounthubTogaPage", () => {
      cy.intercept({
         url:"https://testtogagraphql.azurewebsites.net/api/policygraphql?",
         method:"POST"
      },(req)=>{
           
            if(req.body.variables.input.clause[0].src !== 'r.QuoteNumber'){
               req.alias = "apiRes"
               req.continue()
            }
      })
   cy.get(".actionIcon").first({ timeout: 20000 }).should('be.visible').click({ force: true })
   //wait for apis
   cy.wait("@apiRes",{ timeout: 20000 }).then((res)=>{
       let annualPremium = res.response.body.data.getByFilter[0].InsuredAccount.BusinessInfo.LossHistory[0].AnnualPremium
       accounthubTogaMap.set("annualPremium",annualPremium)
   })
})
//custom command for summary page
Cypress.Commands.add("summaryPage", () => {
   cy.get('[id="summary-tabs-tab-documents"]').click({ force: true })
})
export default { bussinessInfoMap, coveragePageMap, vehicleInfoMap, bussinesinfoTogaMap,accounthubTogaMap,driverinfoTogaMap,operationDetailTogaMap}