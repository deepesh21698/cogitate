// ***********************************************************
// This example support/e2e.js is processed and
// loaded automatically before your test files.
//
// This is a great place to put global configuration and
// behavior that modifies Cypress.
//
// You can change the location of this file or turn off
// automatically serving support files with the
// 'supportFile' configuration option.
//
// You can read more here:
// https://on.cypress.io/configuration
// ***********************************************************

// Import commands.js using ES2015 syntax:
import './commands'
require("cypress-xpath")
//require('cypress-downloadfile/lib/downloadFileCommand');
// cypress/support/index.js or cypress/support/commands.js



// Alternatively you can use CommonJS syntax:
// require('./commands')
import "cypress-localstorage-commands";
import propertyFile from '../fixtures/propertyFile.json'
before(() => {

    cy.clearLocalStorage()
    cy.visit(Cypress.env("endpoint"))
    cy.window().then((win) => {
        win.sessionStorage.clear()
    })
    cy.clearLocalStorageSnapshot();
})

beforeEach(() => {
cy.on('uncaught:exception',()=> false)
    cy.restoreLocalStorage()

});

afterEach(() => {
    cy.saveLocalStorage()
})
after(()=>{
      //delete the file
    // cy.task('deleteFile',`cypress/downloads`)
})

