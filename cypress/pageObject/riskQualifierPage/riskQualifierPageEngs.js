import { RiskQualifier } from "./riskQualifierPage"
class RiskQualifierPageEngs extends RiskQualifier{
    getEngsQuestions(){
        return  cy.get("#pageChangeForm .form-group>label")
    }
}
let riskQualifierPageEngs = new RiskQualifierPageEngs()
export default riskQualifierPageEngs