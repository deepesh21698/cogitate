import { RiskQualifier} from "./riskQualifierPage"
class RiskQualifierPageToga extends RiskQualifier{
    getTogaQuestions(){
        return   cy.get("#pageChangeForm .inputLabel .form-group")
    }
    getNoBtn(){
        return cy.get("//*[text()='No']")
    }
}
let riskQualifierPageToga = new RiskQualifierPageToga()
export default riskQualifierPageToga