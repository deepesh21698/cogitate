
class SummaryPage{
      //bussiness information table
    getBussinessInformationTable(){
        return cy.get(".first-table-para-2")
    }
     //bussiness location table
    getBussinessLocationTable(){
        return  cy.get(".react-bootstrap-table table").first()
    }
    //annually tab
    getAnnualyTab(){
        return cy.get("#annually-tab5")
    }
     //monthly tab
    getMonthlyTab(){
        return cy.get("#monthly-tab4")
    }
    //physically damage value
    getPhysicalDamage(){
        return cy.get("#annually-tab5 .physDamage>div+div")
    }
    //downpayment value
    getDownPayment(){
        return cy.get("#dp_div")
    }
    getDeductibleValue(){
        return cy.get(".compaTableContainer table")
    }
     //bussiness location table
     getVehicleInformationTable(){
        return  cy.get(".react-bootstrap-table table").last()
    }
    //expand btn
    getExpandIcon(){
        return cy.get(".expand-cell")
       }
       //row expand vehicle info
       getExpandedVehicleInfo(){
        return cy.get(".vehicleInfo .row")
       }
       getPrintIcon(){
        return cy.get('[title="Print"]',{timeout:20000})
       }
       getFileDownloadEle(){
        return  cy.get('[id="filedownload"]')
       }
       //common loader
       getLoader(){
        return   cy.get('.modalLoader', { timeout: 200000 })
       }
}

let summaryPage = new SummaryPage()
export default summaryPage