
class SummaryPage{
 getDownloadFileLink(){
    return  cy.get('[title="Download"]')
 }
 getAnnualPremiumValue(){
   return  cy.get(':nth-child(5) > .first-left-table :nth-child(5) > .first-table-para')
 }
 getLicenseNumber(){
   return  cy.get(':nth-child(3) > .d-flex')
 }
}

let summaryPage = new SummaryPage()
export default summaryPage