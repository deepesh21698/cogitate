import { BussinessInfo } from "./bussinessPageInfo"
class BussinessInfoToga extends BussinessInfo {
    //retail agency dropdown
    getRetailAgencyDropdown() {
        return cy.get('input[id="react-select-2-input"]').first()
    }
    getDropdownList() {
        return cy.get(".css-lkh0o5-menu>div>div", { timeout: 10000 })
    }
    getAgentNameDropdown() {
          return cy.xpath('//*[@id="react-select-3-placeholder"]/following-sibling::div /input')
    }
    getUnderwriterDropdown() {
        return cy.xpath('//*[@id="react-select-4-placeholder"]/following-sibling::div /input')
    }
    getUsdotInputField() {
        return cy.get("#usdot")
    }
    getDotSearchBtn() {
        return cy.get("#usDotSearchBtn")
    }
    getBussinessNameField() {
        return cy.get("#businessName")
    }
    getInsuredTypeDropdown() {
        return cy.xpath('//*[@id="react-select-5-placeholder"]/following-sibling::div /input')
    }
    getInsuredTypeList() {
        return cy.xpath("//*[@id='react-select-5-listbox']/div/div", { timeout: 10000 })
    }
    getFirstNameField() {
        return cy.get("#firstname")
    }
    getMiddleNameField() {
        return cy.get("#middlename")
    }
    getLastNameField() {
        return cy.get("#lastname")
    }
    getDbaName() {
        return cy.get("#dbaName")
    }
    getBussinessDescriptionField() {
        return cy.get("#businessDescription")
    }
    getApplicantSubsidiaryDropdown() {
        return cy.xpath('//*[@id="react-select-6-input"]')
    }
    getApplicantSubsidiaryList() {
        return cy.xpath('//*[@id="react-select-6-listbox"]/div/div')
    }
    getEffectiveDateField() {
        return cy.get("#effectivedatecoverage")
    }
    getCalender(){
        return cy.get('.react-calendar__navigation__label')
    }
    getPolicyPeriodDropdown() {
        return cy.get("#policyperiod")
    }
    getCurrentPolicyInfoBtns() {
        return cy.get("#radio-currentInsurancePolicyInfo")
    }
    getInsuredMidTermBtns() {
        return cy.get('[name="btnGrp_isinsuredlookmovemidterm"] >label')
    }
    getPolicyDeclineBtns() {
        return cy.get("#radio-policydeclinthreeyears")
    }
    getMotorCarrierField() {
        return cy.get("#motorcarrier")
    }
    getBussinessStartedDateField() {
        return cy.get("#incorporationDate")
    }
    getYearInBussinessField() {
        return cy.get("#businessAge")
    }
    getOwnershipChangedBtns() {
        return cy.get("#radio-isPastOwnershipChanged")
    }
    getSmsAlertDropdown() {
        return cy.xpath('//*[@id="react-select-7-input"]')
    }
    getSmsAlertList() {
        return cy.xpath('//*[@id="react-select-7-listbox"] /div/div')
    }
    getContactInfoFirstNameField() {
        return cy.get('.formBoxHgt:nth-child(3) #firstname')
    }
    getContactInfoMiddleNameField() {
        return cy.get('.formBoxHgt:nth-child(3) #middlename')
    }
    getContactInfoLastNameField() {
        return cy.get('.formBoxHgt:nth-child(3) #lastname')
    }
    getEmailField() {
        return cy.get("#email")
    }
    getPhoneField() {
        return cy.get("#mobilephone")
    }
    getLocationField() {
        return cy.get('[name="locationTable"] input')
    }
    getCommoditityDropdown() {
        return cy.xpath("//*[@id='react-select-10-input']")
    }
    getCommodityList() {
        return cy.xpath("//*[@id='react-select-10-listbox'] /div/div")
    }
    getHauledField() {
        return cy.get('input[id="PercentageHauled.0"]')
    }
    getMaximumValueField() {
        return cy.get('input[id="MaximumValue.0"]')
    }
    getAverageValueField() {
        return cy.get('input[id="Averagevalue.0"]')
    }
    getTravellingOutStateYesBtn() {
        return cy.get('[name="btn_travellingOutofState0"]')
    }
    getTravellingOutStateNoBtn() {
        return cy.get('[name="btn_travellingOutofState1"]')
    }
    getHowOftenField() {
        return cy.get('input[id="travellingOutofStatehowoften"]')
    }
    getTypeOperationDropdown() {
        return cy.get('input[id="react-select-8-input"]')
    }
    getTypeOperationList() {
        return cy.get('[id="react-select-8-listbox"]>div>div')
    }
    getMajorCitiesTravelledField() {
        return cy.get('[id="citiestravelled"]')
    }
    getStatesDropdown() {
        return cy.get('input[id="react-select-9-input"]')
    }
    getStatesList() {
        return cy.get('[id="react-select-9-group-0-heading"]+div div')
    }
    getRadiusOperationDropdown() {
        return cy.get('input[id="react-select-11-input"]')
    }
    getRadiusOperationList() {
        return cy.get('[id="react-select-11-listbox"]>div div')
    }
    getPercentageField(){
        return cy.get('[id="percentofoperation.0"]')
    }
    getBackBtn(){
        return cy.get('button[id="backBtn"]')
    }
    getSaveForLaterBtn(){
        return cy.get('[id="saveForLaterBtn"][class="btn btnStyle btnPrim"]')
    }

}
let bussinessInfoPage = new BussinessInfoToga()
export default bussinessInfoPage