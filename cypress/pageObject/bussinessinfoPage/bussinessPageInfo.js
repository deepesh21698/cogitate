exports.BussinessInfo = class BussinessInfo {
    getAdressInputField() {
        return cy.get("#react-select-2-input")
    }
    getSavedAddress(){
        return  cy.get(".singleBox table tr")
    }
    getContinueBtn() {
        return cy.get("[type='submit']")
    }
    getSaveBtn() {
        return cy.get(".col-md-3 .btn")
    }
    getAddAddressManuallySwitchBtn() {
        return cy.get("#manualAddress")
    }
    getMailingAddressCheckbox(){
        return cy.xpath('//label[@for="Mailing Address"] /preceding-sibling :: input ')
    }
    
    getGaragingAddressCheckbox(){
        return cy.xpath('//label[@for="Garaging Address"] /preceding-sibling :: input ')
    }
    getModalHeader(){
        return cy.get("#alert .modal-header")
    }
    getModalCloseBtn(){
        return cy.get("#successBTN")
    }
    getSavedAddresses(){
        return cy.get(".singleBox table tbody tr")
    }
    getLoader(){
        return cy.get('.modalLoader', { timeout: 200000 })
    }
}