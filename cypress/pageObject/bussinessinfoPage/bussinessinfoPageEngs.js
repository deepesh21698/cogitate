import { BussinessInfo } from "./bussinessPageInfo"
class BussinessInfoEngs extends BussinessInfo{
getAgentInfoDropdown(){
    return cy.get("#agentdetails")
}
getUnderwriterDropdown(){
    return cy.get("#underwriterdetails")
}
getBussinessNameInputField(){
    return cy.get("#businessname")
}
getBussinessStartedDropdown(){
    return cy.get("#yearofincorporation")
}

getPrimaryGaragingAddressCheckbox(){
    return cy.get("[for='Primary Garaging Address']>input")
}
}
let  bussinessInfoPage = new BussinessInfoEngs()
export default bussinessInfoPage