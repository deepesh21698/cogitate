import { VehicleinfoPage } from "./vehicleInfoPage"
class VehicleinfoPageToga extends VehicleinfoPage {
    getFetchDataBtn() {
        return cy.get('[name="FetchVINButton"]')
    }
    getOverrideVinYesBtn() {
        return cy.get('[name="btn_overridevin0"]')
    }
    getOverrideVinNoBtn() {
        return cy.get('[name="btn_overridevin1"]')
    }
    getVehicleTypeDropdown() {
        return cy.get('[class=" css-1xc3v61-indicatorContainer"]').eq(0)
    }
    getDropdownList() {
        return cy.get('.css-qr46ko>div')
    }
    getSelectedVehicleType() {
        return cy.get('.css-1dimb5e-singleValue').eq(0)
    }
    getYearDropdown() {
        return  cy.get(':nth-child(5) > .custSelect > .css-b62m3t-container > .css-1im77uy-control > .css-hlgwow > .css-19bb58m')
    }
    getYearDropdownList() {
        return cy.get('.css-qr46ko>div')
    }
    getSelectedYear() {
        return cy.get('[class=" css-1dimb5e-singleValue"]').eq(1)
    }
    getStatedValueField() {
        return cy.get('#statedvalue')
    }
    getEstimatedValueField() {
        return cy.get('#annualmileage')
    }
    getMakeDropdown(){
        return  cy.get('[class=" css-1xc3v61-indicatorContainer"]').eq(1)
    }
    getMakeList(){
        return  cy.get('.css-qr46ko>div')
    }
    getGaragingLocation() {
        return cy.xpath('//*[@class=" css-1dimb5e-singleValue"]')
    }
    getOwnershipDropdown() {
        return cy.get('[class=" css-1xc3v61-indicatorContainer"]').eq(4)
    }
    getOwnershipList() {
        return cy.get('.css-qr46ko >div')
    }
    getSelectedOwnership() {
        return cy.get('[class=" css-1dimb5e-singleValue"]').eq(1)
    }
    getTrailorTypeDropdown(){
        return cy.get(':nth-child(12) > .custSelect > .css-b62m3t-container > .css-1im77uy-control > .css-hlgwow > .css-19bb58m')
    }
    getTrailorTypeList(){
        return  cy.get('.css-qr46ko >div')
    }
    getLoggingNoBtn(){
        return  cy.get('[name="btnGrp_electroniclogging"] .btn-buttNo')
    }
    getDetailField(){
        return  cy.get('#nodevice')
    }
    getCameraUnitDropdown(){
        return  cy.get('[class=" css-1xc3v61-indicatorContainer"]').last()
    }
    getCameraUnitList(){
        return  cy.get('.css-qr46ko >div')
    }
    getSubmitBtn(){
        return cy.get('#submitBTN')
    }
    getNxtBtn(){
        return cy.get('#btnnext', { timeout: 30000 })
    }
}
let vehicleinfoPage = new VehicleinfoPageToga()
export default vehicleinfoPage