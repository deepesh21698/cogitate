exports.VehicleinfoPage = class VehicleinfoPage {
  //vehicle vin input field
  getVehicleVinInputField() {
    return cy.get("#vehiclevin")
}
 //model name input field
 getModelInputField() {
  return cy.get("#model")
}
getContinueBtn() {
  return cy.get("[type='submit']")
}
getLoader(){
return  cy.get('.modalLoader', { timeout: 200000 })
}
}
