import { VehicleinfoPage } from "./vehicleInfoPage"
class VehicleinfoPageEngs extends VehicleinfoPage {

    getManualUploadOption() {
        return cy.get('.open-popup-link [alt="Add vehicles Manually"]')
    }
    getVehicleInfoModalBoxHeader() {
        return cy.get(".commonHead")
    }
    //classification type dropdown
    getClassificationTypeDropdown() {
        return cy.get("#classificationtype")
    }
    //vehicle type dropdown
    getVehicleTypeDropdown() {
        return cy.get("#vehicletype")
    }
  
    //year dropdown
    getYearDropdown() {
        return cy.get("#year")
    }
    //make dropdown
    getMakeDropdown() {
        return cy.get("#make")
    }
   
    // size class dropdown
    getSizeClassDropdown() {
        return cy.get("#sizeclass")
    }
    //farthest distance dropdown
    getFarthestDistanceDropdown() {
        return cy.get("#farthestdistance")
    }
    //exposure dropdown
    getExposureDropdown(){
        return cy.get("#exposure")
    }
    //TIV input field
    getTIVInputField(){
        return cy.get("#vehicleprice")
    }
     // service  radio btn
     getBussinessUseServiceRadioBtn(){
        return cy.get("#Service")
     }
      //retail radio btn  
     getBussinessUseRetailRadioBtn(){
        return cy.get("#Retail")
     }
      // commercial radio btn
      getBussinessUseCommercialRadioBtn(){
        return cy.get("#Commercial")
      }
      //passive restraint checkbox
      getPassiveRestraintCheckbox(){
        return cy.get("#passiverestraint")
      }
       //anti lock brake checkbox
       getAntilockBrakeCheckbox(){
        return cy.get("#antilockbrakes")
       }
       //financed radio button
       getFinancedRadioBtn(){
        return cy.get("#Financed")
       }
       //owned radio button
       getOwnedRadioBtn(){
        return cy.get("#Owned")
       }
       //leased radio button
       getLeasedRadioBtn(){
        return cy.get("#Leased")
       }
       //line holder name dropdown
       getLineHolderNameDropdown(){
        return cy.get("#financer")
       }
       //line holder address input field
       getLineHolderAddressInputField(){
        return cy.get("#financerAddress")
       }
       //PD carrier dropdown
       getPDCarrierDropdown(){
        return cy.get("#optedCarrier")
       }
       //cancel btn
       getCancelBtn(){
        return cy.get(".modal-footer .btnReject")
       }
       //submit btn
       getSubmitBtn(){
        return cy.get("#SubmitOnly")
       }
       //submit and add another btn
       getSubmitAddAnotherBtn(){
        return cy.get("#SubmitAddOther")
       }
       //savec successfully popup
       getVehicleInformationSavedPopup(){
        return cy.get('.Toastify__toast-body >div+div')
       }
       //completed information headers
       getCompletedInformationHeaders(){
        return cy.get(".singleBox thead tr th")
       }
       getDeleteVehiclesBtn(){
        return cy.get(".btnClose")
       }
       //completed information value
       getCompletedInformationValue(){
        return cy.get(".selection-cell")
       }
       getExpandIcon(){
        return cy.get(".expand-cell")
       }
       //vehicle information
       getVehicleInformation(){
        return cy.get(".vehicleInfo >div")
       }
       getContinueBtn(){
        return cy.get("[type='submit']")
    }
}

let vehicleInfoPage = new VehicleinfoPageEngs()
export default vehicleInfoPage