class Login {
 //username input filed
 getUsernameInputField() {
    return cy.get("[name='username']")
}
//password input field
getPasswordInputField() {
    return cy.get("[name='password']")
}
//signin button
getSignInBtn() {
    return cy.get("[class='buttonLogin']")
}
  getLoader(){
    return  cy.get('.modalLoader', { timeout: 200000 })

  } 
}
let loginPage = new Login()
export default loginPage