import accounthubPage from "../../pageObject/accounthubPage/accounthubPage"

describe("Verification of login page", () => {
    it("verify login page", () => {
        cy.login(Cypress.env("username"), Cypress.env("password"))
    })
    
})

describe("Verification of accounthub page",()=>{
    it("verify generate quote icon",()=>{
       accounthubPage.getGenerateQuoteIcon().last().should('be.visible').click()
      
    })
    it("verify navigate to risk qualifiers page",()=>{
         //verify navigate to risk qualifiers page
         accounthubPage.getLoader().should('not.exist')

       cy.url().should('include','riskqualifiers')
    })
})