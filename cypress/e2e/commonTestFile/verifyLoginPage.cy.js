//import locators
import loginPage from "../../pageObject/loginPage/loginPage"
//import credentials
//import credentials from '../../fixtures/credentials.json'
//import credentials from '../../fixtures/cypress.env.json'
//import webdata
import webData from '../../fixtures/webData.json'

describe('Verification of login page', () => {
  it('verify login with valid credentials', () => {
   let username = Cypress.env("username")
   let password = Cypress.env("password")
        //enter username
        loginPage.getUsernameInputField().should('be.visible').and('be.enabled').type(username)
        //enter password 
        loginPage.getPasswordInputField().should('be.visible').and('be.enabled').type(password)
        //click on signin btn
        loginPage.getSignInBtn().should('be.enabled').and('have.value', webData.application.cogitate.loginPage.signinBtnTxt).click()
         cy.wait(4000)
  })
  

  it("verify navigate to accounthub page",()=>{
     //verify navigate to accounthub page
     loginPage.getLoader().should('not.exist')
     cy.url().should('include','accounthub')
    
  })
})