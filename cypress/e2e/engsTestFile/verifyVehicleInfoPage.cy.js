//import webdata
import webData from '../../fixtures/webData.json'
//import test data
import testData from '../../fixtures/testData.json'
import credentials from '../../fixtures/credentials.json'
import vehicleInfoPage from '../../pageObject/vehicleInfoPage/vehicleinfoPageEngs'
//import poperty file
import propertyFile from '../../fixtures/propertyFile.json'
import { getRandomNumber } from '../../support/commands'
import { faker } from '@faker-js/faker';
describe("Verification of login page", () => {
    it("verify login", () => {
        cy.login(Cypress.env("username"),Cypress.env("password"))
    })

})
describe("Verification of accounthub page", () => {
    it("verify generate quote icon", () => {
        cy.generateQuoteIcon()
    })

})

describe("Verification of riskqualifiers page", () => {
    it("verify continue btn", () => {
        cy.riskQualifierPageEngs()
     
    })
})
describe("Verification of bussiness info page", () => {
    it("verify required details", () => {
        
        cy.bussinessInfoPageEngs(webData.application.cogitate.engs.bussinessInfoPage.agentNameList[ getRandomNumber(7)], webData.application.cogitate.engs.bussinessInfoPage.underwriterList[getRandomNumber(3)], faker.string.alphanumeric({length:5}), webData.application.cogitate.engs.bussinessInfoPage.yearsList[getRandomNumber(15)], testData.application.cogitate.engs.bussinessInfoPage.address)
    })
})

describe("Verification of vehicle info page", () => {
    it("verify when user clicks on manual upload option then vehicle information modal box should open", () => {
        vehicleInfoPage.getManualUploadOption().then(($uploadBox) => {
            //verify img is visisble
            cy.wrap($uploadBox).should('be.visible')
            //verify manual upload option heading
            cy.wrap($uploadBox).next().should('be.visible').and('contain.text', webData.application.cogitate.engs.vehicleInfoPage.manualUploadOption.heading)
                //verify manual upload option subheading
                .next().should("be.visible").and('contain.text', webData.application.cogitate.engs.vehicleInfoPage.manualUploadOption.subheading)
            //click on the manual upload option
            cy.wrap($uploadBox).click()
            //verify vehicle info modal box appear
            vehicleInfoPage.getVehicleInfoModalBoxHeader().should('be.visible').and("contain.text", webData.application.cogitate.engs.vehicleInfoPage.vehicleInfoModalBoxHeading)
        })
    })
    let classificationType
    it("verify classification type dropdown", () => {
        vehicleInfoPage.getClassificationTypeDropdown().should('be.visible')
            .then(($classification) => {
                //verify label of classification type dropdown
                cy.wrap($classification).parent().parent()
                    .prev().should('contain.text', webData.application.cogitate.engs.vehicleInfoPage.classificationTypeLabel).and('be.visible')
                //select classification type
                cy.wrap($classification).select(webData.application.cogitate.engs.vehicleInfoPage.classificationTypeValue)
                //verify option has selected
                cy.wrap($classification).find('option:selected').should('have.text', webData.application.cogitate.engs.vehicleInfoPage.classificationTypeValue)
                    .invoke('text').then((type) => {
                        classificationType = type.toUpperCase()

                    })
            })
    })
    let vehicleType
    it("verify vehicle type dropdown", () => {
        vehicleInfoPage.getVehicleTypeDropdown().should('be.visible')
            .then(($vehicleType) => {
                cy.wrap($vehicleType).parent().parent()
                    //verify label of vehicle type dropdown
                    .prev().should('contain.text', webData.application.cogitate.engs.vehicleInfoPage.vehicleTypeLabel).and('be.visible')

                //verify vehicle types dropdown list
                cy.wrap($vehicleType).find('option').each(($ele, index) => {
                    expect($ele.text()).equal(webData.application.cogitate.engs.vehicleInfoPage.vehicleTypesdropdown[index])
                })

                switch (propertyFile.application.cogitate.pdCarrier) {
                    case ("GAIC"):
                        //select tank truck vehicle type dropdown
                        cy.wrap($vehicleType).select(webData.application.cogitate.engs.vehicleInfoPage.vehicleTypesdropdown[7])
                        //verify option has selected
                        cy.wrap($vehicleType).find('option:selected').should('have.text', webData.application.cogitate.engs.vehicleInfoPage.vehicleTypesdropdown[7])
                            .invoke('text').then((vehicle) => {
                                vehicleType = vehicle

                            })
                            case("ASSURANT"):
                            cy.wrap($vehicleType).select("Dump Truck")
                            cy.wrap($vehicleType).find('option:selected').should('have.text', "Dump Truck")
                            .invoke('text').then((vehicle) => {
                                vehicleType = vehicle

                            })
                }

            })

    })
    let vinNumber
    it("verify vehicle VIN input field", () => {
        vehicleInfoPage.getVehicleVinInputField()
            .then(($vehicleVinField) => {
                //verify label of vehicle VIN field
                cy.wrap($vehicleVinField).parent()
                    .prev().should('contain.text', webData.application.cogitate.engs.vehicleInfoPage.vehicleVinLabel).and('be.visible')
                //enter vehicle vin
                cy.wrap($vehicleVinField).clear().should('be.enabled')
                    .and('be.visible').and('be.empty').type(testData.application.cogitate.engs.vehicleInfoPage.vehicleVinNum)
                    //verify field has filled
                    .invoke('val').then((vinNum) => {
                        vinNumber = vinNum
                        expect(vinNum).equal(testData.application.cogitate.engs.vehicleInfoPage.vehicleVinNum)
                    })
            })
    })
    let year
    it("verify year dropdown", () => {
        vehicleInfoPage.getYearDropdown().should('be.visible')
            .then(($year) => {
                //verify label of year dropdown
                cy.wrap($year).parent().parent()
                    .prev().find('label').should('contain.text', webData.application.cogitate.engs.vehicleInfoPage.yearLabel).and('be.visible')
                //verify year dropdown list
                cy.wrap($year).find('option').each(($ele, index) => {
                    expect($ele.text()).equal(webData.application.cogitate.engs.vehicleInfoPage.years[index])
                })
                //select year
                cy.wrap($year).select(webData.application.cogitate.engs.vehicleInfoPage.years[6])
                //verify option has selected
                cy.wrap($year).find('option:selected').should('have.text', webData.application.cogitate.engs.vehicleInfoPage.years[6])
                    .invoke('text').then((selectedYear) => {
                        year = selectedYear
                    })

            })
    })
    let make
    it("verify make dropdown", () => {
        //verify make label
        vehicleInfoPage.getMakeDropdown().should('be.visible').parent().parent()
            .prev().should('contain.text', 'MAKE*').and('be.visible')

        if (vehicleType === 'Tank Truck') {
            //verify makes dropdown list
            vehicleInfoPage.getMakeDropdown().find('option').each(($ele, index) => {
                expect($ele.text()).equal(webData.application.cogitate.engs.vehicleInfoPage.makesTankTruckDropdownList[index])
            })
            //select autocar
            vehicleInfoPage.getMakeDropdown().select(webData.application.cogitate.engs.vehicleInfoPage.makesTankTruckDropdownList[3])

            //verify option has selected
            vehicleInfoPage.getMakeDropdown().find('option:selected').should('have.text', webData.application.cogitate.engs.vehicleInfoPage.makesTankTruckDropdownList[3])
                .invoke('text').then((makeValue) => {
                    make = makeValue
                })
        } else if(vehicleType==='Dump Truck'){
                //verify makes dropdown list
                vehicleInfoPage.getMakeDropdown().find('option').each(($ele, index) => {
                    expect($ele.text()).equal(webData.application.cogitate.engs.vehicleInfoPage.makeDumpTruckDropdownList[index])
                })
                //select autocar
                vehicleInfoPage.getMakeDropdown().select(webData.application.cogitate.engs.vehicleInfoPage.makeDumpTruckDropdownList[3])
    
                //verify option has selected
                vehicleInfoPage.getMakeDropdown().find('option:selected').should('have.text', webData.application.cogitate.engs.vehicleInfoPage.makesTankTruckDropdownList[3])
                    .invoke('text').then((makeValue) => {
                        make = makeValue
                    })
        }
    })
    let modelName
    it("verify model name input field", () => {
        vehicleInfoPage.getModelInputField().then(($modelField) => {
            //verify label of model  field
            cy.wrap($modelField).parent().prev().should('be.visible')
                .and('contain.text', webData.application.cogitate.engs.vehicleInfoPage.modelLabel)
            //enter model name
            cy.wrap($modelField).clear().should('be.visible').and('be.enabled').and('be.empty')
                .type(testData.application.cogitate.engs.vehicleInfoPage.modelValue).invoke('val')
                .then((model) => {
                    modelName = model
                    cy.log(model)
                    //verify model name input field is not empty
                    expect(modelName).equal(testData.application.cogitate.engs.vehicleInfoPage.modelValue)
                })
        })
    })
    let sizeClass
    it("verify size classes dropdown", () => {
        vehicleInfoPage.getSizeClassDropdown().should('be.visible').then(($sizeClasses) => {

            //verify label of size classes dropdown
            cy.wrap($sizeClasses).parent().parent()
                .prev().should("contain.text", webData.application.cogitate.engs.vehicleInfoPage.sizeClassLabel).and('be.visible')
            //verify size classes dropdown list
            cy.wrap($sizeClasses).find('option').each(($ele, index) => {
                expect($ele.text()).equal(webData.application.cogitate.engs.vehicleInfoPage.sizeClassesList[index])
            })
            //select the light duty size class
            cy.wrap($sizeClasses).select(webData.application.cogitate.engs.vehicleInfoPage.sizeClassesList[1])
            //verify option has selected
            cy.wrap($sizeClasses).find('option:selected').should('have.text', webData.application.cogitate.engs.vehicleInfoPage.sizeClassesList[1])
                .invoke('text').then((size) => {
                    sizeClass = size
                })
        })
    })
    let farthestDistance
    it("verify farthest distance dropdown", () => {
        vehicleInfoPage.getFarthestDistanceDropdown().should('be.visible').then(($farthestDistance) => {
            //verify label of farthest distance dropdown
            cy.wrap($farthestDistance).parent().parent()
                .prev().should('contain.text', webData.application.cogitate.engs.vehicleInfoPage.farthestDistanceLabel).and('be.visible')
            //verify farthest distance dropdown list
            cy.wrap($farthestDistance).find('option').each(($ele, index) => {
                expect($ele.text()).equal(webData.application.cogitate.engs.vehicleInfoPage.farthestDistanceList[index])
            })
            //select long haul farthest distance
            cy.wrap($farthestDistance).select(webData.application.cogitate.engs.vehicleInfoPage.farthestDistanceList[1])
            //verify option has selected
            cy.wrap($farthestDistance).find('option:selected').should('have.text', webData.application.cogitate.engs.vehicleInfoPage.farthestDistanceList[1])
                .invoke('text').then((distance) => {
                    farthestDistance = distance.trim()
                })
        })
    })
    let exposure
    it("verify exposure dropdown", () => {
        vehicleInfoPage.getExposureDropdown().should('be.visible').then(($exposure) => {
            //verify label of exposure dropdown
            cy.wrap($exposure).parent().parent()
                .prev().should('contain.text', webData.application.cogitate.engs.vehicleInfoPage.exposureLabel).and('be.visible')
            //verify exposure dropdown list
            cy.wrap($exposure).find('option').each(($ele, index) => {
                expect($ele.text()).equal(webData.application.cogitate.engs.vehicleInfoPage.exposureDropdownList[index])
            })
            //select actual cash value from dropdown
            cy.wrap($exposure).select(webData.application.cogitate.engs.vehicleInfoPage.exposureDropdownList[1])
            //verify option has selected
            cy.wrap($exposure).find('option:selected').should('have.text', webData.application.cogitate.engs.vehicleInfoPage.exposureDropdownList[1])
                .invoke('text').then((exposureValue) => {
                    exposure = exposureValue
                })

        })

    })
    let tivNumber
    it("verify TIV input field", () => {

        vehicleInfoPage.getTIVInputField().then(($tiv) => {
            //verify label of TIV input field
            cy.wrap($tiv).parent().prev()
                .should('contain.text', webData.application.cogitate.engs.vehicleInfoPage.tivFieldLabel).and('be.visible')
            //enter TIV number
            cy.wrap($tiv).clear().should('be.enabled').and('be.visible').should('be.empty')
                .type(testData.application.cogitate.engs.vehicleInfoPage.tivNum).invoke('val').then((tivNum) => {
                    tivNumber = tivNum
                    //verify tiv field is not empty
                    expect(tivNum).equal(testData.application.cogitate.engs.vehicleInfoPage.tivNum)
                })
        })

    })
    let bussinessUse
    it("verify bussiness use radio btns", () => {
        vehicleInfoPage.getBussinessUseServiceRadioBtn().then(($radioBtn) => {
            context("verify bussiness use label", () => {
                cy.wrap($radioBtn).parents(".form-group-radio").parent()
                    .prev().should('contain.text', 'BUSINESS USE*').and('be.visible')
            })
        })
        //verify service radio btn
        vehicleInfoPage.getBussinessUseServiceRadioBtn().then(($service) => {
            //verify service radio btn label
            cy.wrap($service).next().next().should('contain.text', 'Service').and('be.visible')
            //verify service radio is checked
            let isSelected = cy.wrap($service).as("service").should('be.checked')
            if (isSelected) {
                bussinessUse = $service.prop('value')
            }
        })
        //verify retail radio btn
        vehicleInfoPage.getBussinessUseRetailRadioBtn().then(($retail) => {
            //verify retail radio btn label
            cy.wrap($retail).next().next().should('contain.text', 'Retail').and('be.visible')
            //verify retail radio btn has not checked
            cy.wrap($retail).should('not.be.checked')
            //check retail radio btn
            cy.wrap($retail).check({ force: true })
            //verify retail radio btn has checked
            let isSelected = cy.wrap($retail).as("retail").should('be.checked')
            if (isSelected) {
                bussinessUse = $retail.prop('value')
            }
            //verify when user clicks on retail radio btn service radio btn should be unchecked
            cy.get("@service").should('not.be.checked')


            //verify commercial radio btn
            vehicleInfoPage.getBussinessUseCommercialRadioBtn().then(($commercial) => {
                //verify commercial radio btn label
                cy.wrap($commercial).next().next().should('contain.text', 'Commercial').and('be.visible')
                //verify commercial radio btn has not checked
                cy.wrap($commercial).should('not.be.checked')
                //check commercial radio btn
                cy.wrap($commercial).check({ force: true })
                //verify commercial radio btn has checked
                const isSelected = cy.wrap($commercial).should('be.checked')
                if (isSelected) {
                    bussinessUse = $commercial.prop('value')
                }
                //verify when user clicks on retail radio btn service radio btn should be unchecked
                cy.get("@retail").should('not.be.checked')
            })

        })
    })
    it("verify vehicle features checkboxes", () => {
        context("verify vehicle features label", () => {
            //verify vehicle features label
            vehicleInfoPage.getPassiveRestraintCheckbox().parent().parent()
                .prev().should('contain.text', 'VEHICLE FEATURES').and('be.visible')
        })
        context("verify passive restraint checkbox", () => {

            //verify passive restraint checkbox
            vehicleInfoPage.getPassiveRestraintCheckbox().then(($passiveCheckbox) => {
                //verify label of passive restraint checkbox
                cy.wrap($passiveCheckbox).next().should('contain.text', 'Passive Restraint').and('be.visible')
                //verify passive restraint checkbox has checked default
                cy.wrap($passiveCheckbox).should('be.checked')
                // uncheck the passive restraint checkbox
                cy.wrap($passiveCheckbox).uncheck({ force: true })
                //verify passive restraint checkbox has unchecked
                cy.wrap($passiveCheckbox).should('not.be.checked')
                // check the passive restraint checkbox
                cy.wrap($passiveCheckbox).check({ force: true })
                //verify passive restraint checkbox has checked
                cy.wrap($passiveCheckbox).should('be.checked')
            })

        })
        context("verify anti lock brakes checkbox", () => {

            vehicleInfoPage.getAntilockBrakeCheckbox().then(($antilockBrakeCheckbox) => {
                //verify label of passive restraint checkbox
                cy.wrap($antilockBrakeCheckbox).next().should('contain.text', 'Anti-Lock Brakes').and('be.visible')
                //verify passive restraint checkbox has checked default
                cy.wrap($antilockBrakeCheckbox).should('be.checked')
                // uncheck the passive restraint checkbox
                cy.wrap($antilockBrakeCheckbox).uncheck({ force: true })
                //verify passive restraint checkbox has unchecked
                cy.wrap($antilockBrakeCheckbox).should('not.be.checked')
                // check the passive restraint checkbox
                cy.wrap($antilockBrakeCheckbox).check({ force: true })
                //verify passive restraint checkbox has checked
                cy.wrap($antilockBrakeCheckbox).should('be.checked')
            })

        })

    })
    let ownership
    it("verify ownership radio btns", () => {

        context("verify ownership label", () => {
            vehicleInfoPage.getFinancedRadioBtn().parents(".form-group-radio")
                .parent().prev().should('contain.text', 'OWNERSHIP*').and('be.visible')
        })
        context("verify financed radio btn", () => {
            vehicleInfoPage.getFinancedRadioBtn().then(($financed) => {
                //verify financed checbox label
                cy.wrap($financed).next().next().should('contain.text', 'Financed')
                //verify financed checbox has checked by default
                const isCheck = cy.wrap($financed).as("financed").should('be.checked')
                if (isCheck) {
                    const prop = $financed.prop('value')
                    ownership = prop.toUpperCase()

                }
            })
        })
        context("verify owned radio btn", () => {
            vehicleInfoPage.getOwnedRadioBtn().then(($owned) => {
                //verify owned checbox label
                cy.wrap($owned).next().next().should('contain.text', 'Owned')
                //verify owned checbox has not checked by default
                cy.wrap($owned).should('not.be.checked')
                //check the owned checkbox
                cy.wrap($owned).check({ force: true })
                //verify owned checbox has checked
                let isCheck = cy.wrap($owned).as("owned").should('be.checked')
                if (isCheck) {
                    const prop = $owned.prop('value')
                    ownership = prop

                }
                //verify financed radio btn unchecked when click on owned radio btn
                cy.get("@financed").should('not.be.checked')

            })
        })
        context("verify leased radio btn", () => {
            vehicleInfoPage.getLeasedRadioBtn().then(($leased) => {
                //verify leased checbox label
                cy.wrap($leased).next().next().should('contain.text', 'Leased')
                //verify leased checbox has not checked by default
                cy.wrap($leased).should('not.be.checked')
                //check the leased checkbox
                cy.wrap($leased).check({ force: true })
                //verify leased checbox has checked
                const isCheck = cy.wrap($leased).should('be.checked')
                if (isCheck) {
                    const prop = $leased.prop('value')
                    ownership = prop

                }
                //verify owned radio btn unchecked when click on leased radio btn
                cy.get("@owned").should('not.be.checked')

                //check the financed radio btn
                cy.get("@financed").check({ force: true })
                cy.get("@financed").should('be.checked')

                //verify leased checbox has unchecked
                cy.wrap($leased).should('not.be.checked')

            })
        })
    })
    it("verify line holder name and line holder fields should not be visible when user check owned radio btn", () => {
        //check owned radio button   
        vehicleInfoPage.getOwnedRadioBtn().check({ force: true })
        //verify line holder dropdown not exist
        vehicleInfoPage.getLineHolderNameDropdown().should('not.exist')
        //verify line holder field not exist
        vehicleInfoPage.getLineHolderAddressInputField().should('not.exist')
    })
    it("verify line holder name and line holder fields should be visible when user check financed or leased radio btn", () => {
        //check financed radio button   
        vehicleInfoPage.getFinancedRadioBtn().should('not.be.checked').check({ force: true })
        //verify line holder dropdown not exist
        vehicleInfoPage.getLineHolderNameDropdown().should('be.visible')
        //verify line holder field not exist
        vehicleInfoPage.getLineHolderAddressInputField().should('be.visible')
        //check financed radio button   
        vehicleInfoPage.getLeasedRadioBtn().should('not.be.checked').check({ force: true })
        //verify line holder dropdown not exist
        vehicleInfoPage.getLineHolderNameDropdown().should('be.visible')
        //verify line holder field not exist
        vehicleInfoPage.getLineHolderAddressInputField().should('be.visible')
    })
    let lineHolderName

    it("verify line holder dropdown", () => {
        vehicleInfoPage.getLineHolderNameDropdown().should('be.visible').then(($lineHolder) => {
            //verify line holder label
            cy.wrap($lineHolder).parent().parent()
                .prev().should('contain.text', 'LIENHOLDER NAME').and('be.visible')
            //verify line holder dropdown list
            cy.wrap($lineHolder).find('option').each(($ele, index) => {
                expect($ele.text()).equal(webData.application.cogitate.engs.vehicleInfoPage.lineHolderNameDropdownList[index])
            })
            //select the ENGS Finance Company 1 option
            cy.wrap($lineHolder).select(webData.application.cogitate.engs.vehicleInfoPage.lineHolderNameDropdownList[1])
            //verify option has selected
            cy.wrap($lineHolder).find('option:selected').should('have.text', webData.application.cogitate.engs.vehicleInfoPage.lineHolderNameDropdownList[1])
                .invoke('text').then((holderName) => {
                    lineHolderName = holderName
                })
        })
    })
    let lineHolderAddress
    it("verify line holder address field", () => {
        vehicleInfoPage.getLineHolderAddressInputField().then(($lineHolder) => {
            //verify line holder field label
            cy.wrap($lineHolder).parent().prev()
                .should('contain.text', 'LIENHOLDER ADDRESS').and('be.visible')
            //verify field contain some value
            cy.wrap($lineHolder).should('be.enabled').and('be.visible')
                .invoke('text').then((holderAddress) => {
                    lineHolderAddress = holderAddress
                })
        })

    })
let pdCarrier
    it("verify PD carrier dropdown", () => {
        vehicleInfoPage.getPDCarrierDropdown().should('be.visible').then(($pdCarrier) => {
            //verify label of pd carrier dropdwon
            cy.wrap($pdCarrier).parent().parent().prev().should('contain.text', 'PD CARRIER*')
            //verify dropdown list
            cy.wrap($pdCarrier).find('option').each(($ele, index) => {
                expect($ele.text()).equal(webData.application.cogitate.engs.vehicleInfoPage.pdCarrierDropdownList[index])
            })
            //select pd carrier GAIC
            cy.wrap($pdCarrier).select(propertyFile.application.cogitate.pdCarrier)

            //verify option has selected
            cy.wrap($pdCarrier).find('option:selected').should('have.text', propertyFile.application.cogitate.pdCarrier)
                .invoke('text').then((carrier) => {
                    pdCarrier = carrier
                })

        })
    })
    it("verify vehicle info modal footer", () => {
        //verify cancel btn
        vehicleInfoPage.getCancelBtn().should('be.visible').and('be.enabled').and('contain.text', 'CANCEL')
        //verify submit and add another btn
        vehicleInfoPage.getSubmitAddAnotherBtn().should('be.visible').and('be.enabled').and('contain.text', 'SUBMIT AND ADD ANOTHER')
        //verify submit radio button
        vehicleInfoPage.getSubmitBtn().should('be.visible').and('be.enabled').and('contain.text', 'SUBMIT').click()
    })

    it("verify vehicle information saved succesfully", () => {
        vehicleInfoPage.getVehicleInformationSavedPopup()
            .should('be.visible')
            .and('contain.text', webData.application.cogitate.engs.vehicleInfoPage.vehicleSavedMessage)
    })

    it("verify completed information", () => {
        context("verify completed information headings", () => {
            //verify headers
            vehicleInfoPage.getCompletedInformationHeaders().then(($headers) => {
                cy.wrap($headers).not('.selection-cell-header').each(($ele, index) => {
                    cy.wrap($ele).should('be.visible').and('contain.text', webData.application.cogitate.engs.vehicleInfoPage.completedInformationHeaders[index])
                })
            })
        })
        context("verify completed information value", () => {

            vehicleInfoPage.getCompletedInformationValue().then(($ele) => {
                //verify checkbox
                cy.wrap($ele).children().should('be.visible').and('not.be.checked').check()
                cy.wrap($ele).children().should('be.checked').uncheck()
                //verify make/model/year
                cy.wrap($ele).next()
                    .children('div')
                    //verify img is visible
                    .find('img').should('be.visible')
                    .next().should('contain.text', make + modelName + year)
                    //verify VIN
                    .parents('.success').next().should('contain.text', vinNumber)
                    //verify vehicle type
                    .next().should('contain.text', vehicleType.toUpperCase() + classificationType + ownership)
                    //verify carrier
                    .next().should('contain.text', pdCarrier)
            })
        })
    })

    it("verify vehicle information ", () => {
        //expand the vehicle information details
        vehicleInfoPage.getExpandIcon().click()
        vehicleInfoPage.getVehicleInformation().then(($vehicleInformation) => {
            //verify size class weight exposure
            cy.wrap($vehicleInformation).eq(0).children()
                //verify size class label
                .first().should('contain.text', 'SIZE CLASS(WEIGHT)').and('be.visible')
                //verify size class value
                .next().should('contain.text', sizeClass).and('be.visible')
                //verify exposure label
                .next().should('contain.text', 'EXPOSURE').and('be.visible')
                //verify exposure value
                .next().should('contain.text', exposure).and('be.visible')

            //verify gross combination weight and  vehicle price
            cy.wrap($vehicleInformation).eq(1).children()
                //verify label of gross combination
                .first().should('contain.text', 'GROSS COMBINATION WEIGHT').and('be.visible')
                //verify gross combination value
                .next().should('contain.text', "---").and('be.visible')
                //verify vehicle price label
                .next().should('contain.text', 'VEHICLE PRICE').and('be.visible')
                //verify vehicle price value
                .next().should('contain.text', `$${tivNumber}`).and('be.visible')


            //verify bussiness use and farthest distance
            cy.wrap($vehicleInformation).eq(2).children()
                //verify label of bussiness use
                .first().should('contain.text', 'BUSINESS USE').and('be.visible')
                //verify bussiness use value
                .next().should('contain.text', bussinessUse).and('be.visible')
                //verify label of farthest distance
                .next().should('contain.text', 'FARTHEST DISTANCE').and('be.visible')
                //verify  farthest distance value
                .next().find('p').then(($farthestDistance) => {
                    //   expect($farthestDistance.text().trim()).includes(farthestDistance.trim().toUpperCase())
                })

            //verify line holder name and line holder address
            cy.wrap($vehicleInformation).eq(3).children()
                //verify label of holder name
                .first().should('contain.text', 'LIENHOLDER NAME').and('be.visible')
                //verify holder name value
                .next().should('contain.text', lineHolderName.toUpperCase()).and('be.visible')
                //verify label of line holder address
                .next().should('contain.text', 'LIENHOLDER ADDRESS').and('be.visible')
                //verify line holder value value
                .next().should('contain.text', lineHolderAddress).and('be.visible')

        })
        //click on continue btn
        vehicleInfoPage.getContinueBtn().should('be.visible').and('be.enabled').click()
    })
    it("verify navigate to coverage info page", () => {
        cy.wait(3000)
        cy.url().should('include', 'coverageinfo')
    })
})