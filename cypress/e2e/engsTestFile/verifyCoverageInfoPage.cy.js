
//import webdata
import webData from '../../fixtures/webData.json'
//import test data
import testData from '../../fixtures/testData.json'
import coverageInfoPage from '../../pageObject/coverageInfoPage/coverageInfoPage'
import propertyFile from '../../fixtures/propertyFile.json'
import { getRandomNumber } from '../../support/commands'
import { faker } from '@faker-js/faker';
describe("Verification of login page", () => {
    it("verify login", () => {
        cy.login(Cypress.env("username"),Cypress.env("password"))
     })

})
describe("Verification of accounthub page", () => {
    it("verify generate quote icon", () => {
        cy.generateQuoteIcon()
    })

})

describe("Verification of riskqualifiers page", () => {
    it("verify continue btn", () => {
        cy.riskQualifierPageEngs()
    })
})
describe("Verification of bussiness info page", () => {
    it("verify required details", () => {
        
        cy.bussinessInfoPageEngs(webData.application.cogitate.engs.bussinessInfoPage.agentNameList[ getRandomNumber(7)], webData.application.cogitate.engs.bussinessInfoPage.underwriterList[getRandomNumber(3)], faker.lorem.word(), webData.application.cogitate.engs.bussinessInfoPage.yearsList[getRandomNumber(15)], testData.application.cogitate.engs.bussinessInfoPage.address)
    })
})

describe("Verification of vehicle info page", () => {
    it("verify vehicle information", () => {
        cy.vehicleInfoPageEngs(faker.string.binary({length:17}),webData.application.cogitate.engs.vehicleInfoPage.years[getRandomNumber(15)], webData.application.cogitate.engs.vehicleInfoPage.makeDumpTruckDropdownList[getRandomNumber(30)], faker.string.alphanumeric({casing:'upper',length:5}),webData.application.cogitate.engs.vehicleInfoPage.sizeClassesList[getRandomNumber(3)], webData.application.cogitate.engs.vehicleInfoPage.farthestDistanceList[getRandomNumber(2)], webData.application.cogitate.engs.vehicleInfoPage.exposureDropdownList[getRandomNumber(2)], faker.number.int({min:5000,max:10000}),webData.application.cogitate.engs.vehicleInfoPage.lineHolderNameDropdownList[getRandomNumber(4)], propertyFile.application.cogitate.pdCarrier)

    })
})
describe("Verification of coverage info page", () => {
    it("verify deductibles dropdown", () => {
        coverageInfoPage.getDeductibleDropdown().should('be.visible').then(($deductibleValue) => {

            //get label text
            coverageInfoPage.getDeductibleDropdownLabel().invoke('text').then((carrierLabel) => {

                if (carrierLabel.includes("GAIC")) {
                    //verify GAIC deductible dropdown list
                    cy.wrap($deductibleValue).find('option').each(($ele, index) => {
                        expect($ele.text()).contain(webData.application.cogitate.engs.coverageInfoPage.gaicdeductiblePrice[index])
                    })
                    //select the $2,500 value
                    cy.wrap($deductibleValue).select(webData.application.cogitate.engs.coverageInfoPage.deductiblePrice1)
                    //verify $2,500 value has selected
                    cy.wrap($deductibleValue).find('option:selected').should('have.text', webData.application.cogitate.engs.coverageInfoPage.deductiblePrice1)
                }
                else if (carrierLabel.includes("ASSURANT")) {
                    //verify assurant deductible dropdown list
                    cy.wrap($deductibleValue).find('option').each(($ele, index) => {
                        expect($ele.text()).contain(webData.application.cogitate.engs.coverageInfoPage.assurantdeductiblePrice[index])
                    })
                    //select the $2,500 value
                    cy.wrap($deductibleValue).select(webData.application.cogitate.engs.coverageInfoPage.deductiblePrice1)
                    //verify $2,500 value has selected
                    cy.wrap($deductibleValue).find('option:selected').should('have.text', webData.application.cogitate.engs.coverageInfoPage.deductiblePrice1)
                }
            })


        })
        //click on continue btn
        coverageInfoPage.getContinueBtn().click()
  

    })
    it("verify navigate to summary page",()=>{
        cy.url().should('include','summary')
    })
})
