
//import webdata
import webData from '../../fixtures/webData.json'
//import test data
import testData from '../../fixtures/testData.json'
import bussinessInfoPage from '../../pageObject/bussinessinfoPage/bussinessinfoPageEngs'
describe("Verification of login page", () => {
    it("verify login", () => {
        cy.login(Cypress.env("username"),Cypress.env("password"))
    })

})
describe("Verification of accounthub page", () => {
    it("verify generate quote icon", () => {
        cy.generateQuoteIcon()
        
    })

})

describe("Verification of riskqualifiers page", () => {
    it("verify continue btn", () => {

        cy.riskQualifierPageEngs()
    })
})

describe("Verification of bussinesinfo page", () => {

    it("verify Agency & Underwriter Details dropdown should be selectable", () => {
        //select agent
        bussinessInfoPage.getAgentInfoDropdown().should('be.visible')
            .then(($agentDropdown) => {
                cy.wrap($agentDropdown).select(webData.application.cogitate.engs.bussinessInfoPage.agentName, { force: true })
                cy.wrap($agentDropdown).find('option:selected').should('have.text', webData.application.cogitate.engs.bussinessInfoPage.agentName)
                    
            })
        //select underwriter
        bussinessInfoPage.getUnderwriterDropdown().should('be.visible').
            then(($underwriterDropdown) => {
                cy.wrap($underwriterDropdown).select(webData.application.cogitate.engs.bussinessInfoPage.underwriter, { force: true })

                cy.wrap($underwriterDropdown).find('option:selected').should('have.text', webData.application.cogitate.engs.bussinessInfoPage.underwriter)
                  
            })

    })

    it("verify bussiness information fields should be selectable", () => {

        //enter bussiness name
        bussinessInfoPage.getBussinessNameInputField().clear()
            .should('be.visible').and('be.empty').and('be.enabled')
            .type(testData.application.cogitate.engs.bussinessInfoPage.bussinessName)
        //select started year of bussiness
        bussinessInfoPage.getBussinessStartedDropdown().should('be.visible')
            .then(($startedYear) => {
                cy.wrap($startedYear).select(webData.application.cogitate.engs.bussinessInfoPage.year)
                //assert value has selected
                cy.wrap($startedYear).find('option:selected').should('have.text', webData.application.cogitate.engs.bussinessInfoPage.year)
                  
            })

    })

    it("verify user is able to save the location", () => {

        //enter address
        bussinessInfoPage.getAdressInputField().should('be.visible')
            .and('be.enabled').then(($address) => {
                cy.wrap($address).type(testData.application.cogitate.engs.bussinessInfoPage.address, { force: true })
                cy.wait(2000)
                //enter from keyboard
                cy.wrap($address).type('{enter}')
            })

        //verify mailing address check box
        bussinessInfoPage.getMailingAddressCheckbox().then(($mailingAddressCheckbox) => {
            cy.wrap($mailingAddressCheckbox).should('be.checked')
            cy.wait(2000)
            cy.wrap($mailingAddressCheckbox).uncheck({ force: true })
            cy.wrap($mailingAddressCheckbox).should('not.be.checked')
            cy.wait(2000)
            cy.wrap($mailingAddressCheckbox).check({ force: true })
            cy.wrap($mailingAddressCheckbox).should('be.checked')

        })
        //verify primary garage checkbox
        bussinessInfoPage.getPrimaryGaragingAddressCheckbox().then(($primaryGaragingCheckbox) => {
            cy.wrap($primaryGaragingCheckbox).should('be.checked')
            cy.wait(2000)
            cy.wrap($primaryGaragingCheckbox).uncheck({ force: true })
            cy.wrap($primaryGaragingCheckbox).should('not.be.checked')
            cy.wait(2000)
            cy.wrap($primaryGaragingCheckbox).check({ force: true })
            cy.wrap($primaryGaragingCheckbox).should('be.checked')

        })
        //verify garaging address checkbox
        bussinessInfoPage.getGaragingAddressCheckbox().then(($garagingAddressCheckbox) => {
            cy.wrap($garagingAddressCheckbox).should('not.be.checked')
            cy.wait(2000)
            cy.wrap($garagingAddressCheckbox).check({ force: true })
            cy.wrap($garagingAddressCheckbox).should('be.checked')
            cy.wait(2000)
            cy.wrap($garagingAddressCheckbox).uncheck({ force: true })
            cy.wrap($garagingAddressCheckbox).should('not.be.checked')
        })
        //save the address
        bussinessInfoPage.getSaveBtn().should('be.visible').and('contain.text', 'SAVE').click()
        //verify address should be saved
       bussinessInfoPage.getSavedAddress().then(($address) => {
            cy.wrap($address).should('have.length', '3')
        })

        //submit the btn
        bussinessInfoPage.getContinueBtn().should('contain.text', 'CONTINUE').click()

    })
    it("verify navigate to vehicle info page", () => {
        cy.url().should('include', 'vehicleinfo')
    })
})

