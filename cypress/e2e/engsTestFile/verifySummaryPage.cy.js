

//import webdata
import webData from '../../fixtures/webData.json'
//import test data
import testData from '../../fixtures/testData.json'
import credentials from '../../fixtures/credentials.json'
import propertyFile from '../../fixtures/propertyFile.json'
import summaryPage from '../../pageObject/summaryPage/summaryPageEngs'
import map from '../../support/commands'
import { getPolicyEffectiveDate } from '../../support/commands'
import { getPolicyExpirationDate } from '../../support/commands'
import { ownership } from '../../support/commands'
import { bussinessUse } from '../../support/commands'
import { getRandomNumber } from '../../support/commands'
import { faker } from '@faker-js/faker';
describe("Verification of login page", () => {
    it("verify login", () => {
        cy.login(Cypress.env("username"),Cypress.env("password"))
     })

})
describe("Verification of accounthub page", () => {
    it("verify generate quote icon", () => {
        cy.generateQuoteIcon()
    })
})

describe("Verification of riskqualifiers page", () => {
    it("verify continue btn", () => {
        cy.riskQualifierPageEngs()
    })
})
describe("Verification of bussiness info page", () => {
    it("verify required details", () => {
        
        cy.bussinessInfoPageEngs(webData.application.cogitate.engs.bussinessInfoPage.agentNameList[ getRandomNumber(7)], webData.application.cogitate.engs.bussinessInfoPage.underwriterList[getRandomNumber(3)], faker.string.alphanumeric({length:5}), webData.application.cogitate.engs.bussinessInfoPage.yearsList[getRandomNumber(15)], testData.application.cogitate.engs.bussinessInfoPage.address)
    })
})

describe("Verification of vehicle info page", () => {
    it("verify vehicle information", () => {
        cy.vehicleInfoPageEngs(faker.string.binary({length:17}),webData.application.cogitate.engs.vehicleInfoPage.years[getRandomNumber(15)], webData.application.cogitate.engs.vehicleInfoPage.makeDumpTruckDropdownList[getRandomNumber(30)], faker.string.alphanumeric({casing:'upper',length:5}),webData.application.cogitate.engs.vehicleInfoPage.sizeClassesList[getRandomNumber(3)], webData.application.cogitate.engs.vehicleInfoPage.farthestDistanceList[getRandomNumber(2)], webData.application.cogitate.engs.vehicleInfoPage.exposureDropdownList[getRandomNumber(2)], faker.number.int({min:1000,max:10000}),webData.application.cogitate.engs.vehicleInfoPage.lineHolderNameDropdownList[getRandomNumber(4)], propertyFile.application.cogitate.pdCarrier)

    })
})
describe("Verification of coverage info page", () => {
    it("verify deductibles dropdown", () => {
        cy.coverageInfoPageEngs(webData.application.cogitate.engs.coverageInfoPage.deductiblePrice1)
    })
})
export let annuallyPhysicalDamageValue
export let monthlyPhysicalDamageValue
describe("Verification of summary page", () => {

    it("verify primary selector", () => {
        let expectedDownPayment
        context("verify monthly physical damage value ", () => {
            summaryPage.getPhysicalDamage().should('be.visible').then(($physicalDamage) => {
                let physicalDamage = $physicalDamage.text().replace("$", "")
                annuallyPhysicalDamageValue = parseFloat(physicalDamage.replace("/yearly", "")).toFixed(2)
                monthlyPhysicalDamageValue = parseFloat(annuallyPhysicalDamageValue / 12).toFixed(2)
                //click on monthly tab
                summaryPage.getMonthlyTab().should('contain.text', "Monthly").scrollIntoView().click({ force: true })

                cy.wrap($physicalDamage).should('be.visible').then(($monthlyValue) => {
                    const monthlyValue = $monthlyValue.text().replace("$", "")
                    let expectedMonthlyDamageValue = monthlyValue.replace("/monthly", "")
                    //verify monthly physical damage value
                    expect(expectedMonthlyDamageValue).contains(monthlyPhysicalDamageValue)
                    expectedDownPayment = parseFloat(expectedMonthlyDamageValue) + parseFloat(expectedMonthlyDamageValue)
                })
            })

            context("verify down payment value", () => {
                //verify down payment value
                summaryPage.getDownPayment().should('be.visible').then(($downPayment) => {
                    const actualDownpayment = $downPayment.text().replace("$", "")
                    expect(parseFloat(expectedDownPayment)).equal(parseFloat(actualDownpayment))

                })
            })
        })
    })
    let deductibleValue
    it("verify policy deductible", () => {
        summaryPage.getDeductibleValue().should('be.visible').then(($deductible) => {
            //verify heading
            cy.wrap($deductible).find('th').should('contain.text', "Policy Deductibles")
            cy.wrap($deductible).find('tr').last()
                //verify deductible amount
                .children().first().next().then(($deductibleValue) => {
                    deductibleValue = $deductibleValue.text()
                    expect(deductibleValue).equal(map.coveragePageMap.get("deductibleAmount"))
                })

        })
    })


    it("verify bussiness information block details", () => {
        summaryPage.getBussinessInformationTable().then(($bussinessInfo) => {
            //verify label of bussiness name
            cy.wrap($bussinessInfo).eq(0).should('contain.text', 'Business Name').and('be.visible')
                //verify value of bussiness name
                .next().should('contain.text', map.bussinessInfoMap.get("bussinessName")).and('be.visible')
            //verify label of year
            cy.wrap($bussinessInfo).eq(1).should('contain.text', 'Year of Incorporation').and('be.visible')
                //verify value of year
                .next().should('contain.text', map.bussinessInfoMap.get("year")).and('be.visible')
            //verify policy effective date
            cy.wrap($bussinessInfo).eq(2).should('contain.text', 'Policy Effective Date').and('be.visible')
                //verify value of policy effective
                .next().should('contain.text', getPolicyEffectiveDate()).and('be.visible')
            //verify policy expiration date
            cy.wrap($bussinessInfo).eq(3).should('contain.text', 'Policy Expiration Date').and('be.visible')
                //verify value of policy expiration
                .next().should('contain.text', getPolicyExpirationDate()).and('be.visible')
            //verify label of agent
            cy.wrap($bussinessInfo).eq(4).should('contain.text', 'Agent').and('be.visible')
                //verify value of agent
                .next().should('contain.text', map.bussinessInfoMap.get("agent")).and('be.visible')
            //verify label of agent
            cy.wrap($bussinessInfo).eq(5).should('contain.text', 'Underwriter').and('be.visible')
                //verify value of agent
                .next().should('contain.text', map.bussinessInfoMap.get("underwriter")).and('be.visible')
        })

    })
    it("verify bussiness location details", () => {
        summaryPage.getBussinessLocationTable().then(($location) => {
            //verify heading of bussiness location
            cy.wrap($location).find('th').first().should("contain.text", "TYPE").and('be.visible')
                .next().should("contain.text", "ADDRESS").and('be.visible')

            //verify mailing address
            cy.wrap($location).find('tbody').children().first()
                .find('td').first().should("contain.text", "Mailing Address").and('be.visible')
                .next().invoke('text').then((address) => {
                    expect(address.toUpperCase()).equal(map.bussinessInfoMap.get("mailingAddress"))
                })
            //verify primary garaging address
            cy.wrap($location).find('tbody').children().last()
                .find('td').first().should("contain.text", "Primary Garaging Address").and('be.visible')
                .next().invoke('text').then((address2) => {
                    expect(address2.toUpperCase()).equal(map.bussinessInfoMap.get("garagingAddress"))
                })
        })
    })
    it("verify vehicle information headers and theirs values", () => {

        //verify vehicle information headers
        summaryPage.getVehicleInformationTable().should('be.visible').then(($vehicleInformation) => {
            cy.wrap($vehicleInformation).find('th').each(($heading, index) => {
                cy.wrap($heading).should('contain.text', webData.application.cogitate.engs.summaryPage.vehicleHeadings[index])

            })
            //verify YEAR/MAKE/MODEL value
            cy.wrap($vehicleInformation).find('tbody').children('tr').first()
                .children().first()
                .should("contain.text", `${map.vehicleInfoMap.get("year")}/${map.vehicleInfoMap.get('make')}/${map.vehicleInfoMap.get("modelName")}`).and('be.visible')
                //verify vin number
                .next().should('contain.text', map.vehicleInfoMap.get("vinNumber")).and('be.visible')
                //verify classification
                .next().should('contain.text', map.vehicleInfoMap.get("classificationType").toUpperCase()).and('be.visible')
                //verify vehicle type
                .next().should('contain.text', map.vehicleInfoMap.get("vehicleType").toUpperCase()).and('be.visible')
                //verify pd carrier
                .next().should('contain.text', propertyFile.application.cogitate.pdCarrier).and('be.visible')
                //verify TIV
                .next().as("tiv").then(($tiv) => {
                    let tiv = map.vehicleInfoMap.get("tiv")
                    let tiv1 = tiv.charAt(0)
                    let newTiv = tiv1.concat(',')
                    let expectedTivValue = newTiv + tiv.slice(1)
                    cy.wrap($tiv).should('contain.text', "$" + expectedTivValue).and('be.visible')

                })
            //verify annually physical damage value
            cy.get("@tiv").next().should('contain.text', `$${annuallyPhysicalDamageValue}`).and('be.visible')
                //verify monthly physical damage value
                .next().should('contain.text', `$${parseFloat(monthlyPhysicalDamageValue).toFixed(2)}`).and('be.visible')
            //expand the cell
            summaryPage.getExpandIcon().should('be.visible').click()
        })
    })

    it("verify vehicle information after expand", () => {
        summaryPage.getExpandedVehicleInfo().should('be.visible').then(($vehicleInfo) => {
            context("verify size class and exposure", () => {
                cy.wrap($vehicleInfo).eq(0).then(($row1) => {
                    cy.wrap($row1).children()
                        //verify size class label
                        .first().should('contain.text', "SIZE CLASS(WEIGHT)").and('be.visible')
                        //verify size class value
                        .next().should('contain.text', map.vehicleInfoMap.get("sizeClass")).and('be.visible')
                        //verify exposure label
                        .next().should('contain.text', 'EXPOSURE').and('be.visible')
                        //verify exposure value
                        .next().scrollIntoView().should('contain.text', map.vehicleInfoMap.get("exposure")).and('be.visible')
                })
            })
            context("verify gross combination weight and owned/leased", () => {
                cy.wrap($vehicleInfo).eq(1).then(($row2) => {
                    cy.wrap($row2).children()
                        //verify gross combination weight label
                        .first().should('contain.text', "GROSS COMBINATION WEIGHT").and('be.visible')
                        //verify ross combination weight value
                        .next().should('contain.text', "---").and('be.visible')
                        //verify owned/leased label
                        .next().should('contain.text', 'OWNED/LEASED').and('be.visible')
                        //verifyowned/leased value
                        .next().scrollIntoView().should('contain.text', ownership).and('be.visible')

                })
            })
            context("verify bussiness use and vehicle feature", () => {
                cy.wrap($vehicleInfo).eq(2).then(($row2) => {
                    cy.wrap($row2).children()
                        //verify bussiness use label
                        .first().should('contain.text', "BUSINESS USE SERVICE").and('be.visible')
                        //verify bussiness use value
                        .next().should('contain.text', bussinessUse).and('be.visible')
                        //verify vehicle feature label
                        .next().should('contain.text', 'VEHICLE FEATURES').and('be.visible')
                        //verify vehicle feature value
                        .next().scrollIntoView().should('contain.text', "Anti-Lock Brakes & Passive Restraint").and('be.visible')

                })
            })
            context("verify farthest distance and line holder name", () => {
                cy.wrap($vehicleInfo).eq(3).then(($row3) => {
                    cy.wrap($row3).children()
                        //verify farthest distance label
                        .first().should('contain.text', "FARTHEST DISTANCE").and('be.visible')
                        //verify farthest distance value
                        .next().as("farthest").then(($farthest) => {
                            let actualFarthestDistance = $farthest.text()
                            let expectedFarthestDistance = map.vehicleInfoMap.get("farthestDistance").toUpperCase()
                            expectedFarthestDistance = expectedFarthestDistance.replace("> ", ">")
                            expect(actualFarthestDistance).equal(expectedFarthestDistance)

                        })
                    //verify line holder name label
                    cy.get("@farthest").next().should('contain.text', 'LIENHOLDER NAME').and('be.visible')
                        //verify line holder name value
                        .next().scrollIntoView().should('contain.text', map.vehicleInfoMap.get("lineHolderName").toUpperCase()).and('be.visible')

                    cy.log(map.vehicleInfoMap.get("lineHolderName"))

                })
            })

            context("verify line holder address", () => {
                cy.wrap($vehicleInfo).eq(4).then(($row3) => {
                    cy.wrap($row3).children()
                        //verify farthest distance label
                        .should('contain.text', "LIENHOLDER ADDRESS").and('be.visible')
                        //verify farthest distance value
                        .next().scrollIntoView().should('have.text', map.vehicleInfoMap.get("lineHolderAddress")).and('be.visible')


                })
            })
        })

    })
    it("verify annual premium in pdf", () => {
        //download the file
       summaryPage.getPrintIcon().should('be.visible').click({force:true})
       //wait for exist
       summaryPage.getLoader().should('not.exist')
        //get download ele
        summaryPage.getFileDownloadEle().then(($file)=>{
            const src = $file.prop('download')
            //read the pdf
            cy.task('getPdf',`cypress/downloads/${src}`).then((data)=>{
                const pdfPremium = data.pages[0].texts[3].text
                const pdfAnnualyPhysicalDamageValue = pdfPremium.replace("annually","").replace("$","").trim()
                //verify physical damage value annually
                expect(annuallyPhysicalDamageValue).contain(pdfAnnualyPhysicalDamageValue)
                //verify make
                const make = data.pages[0].texts[29].text
                cy.wrap(map.vehicleInfoMap.get("make")).should('eq',make)
             })
        })
    })
    it("verify annual premium with api",()=>{
        expect(JSON.stringify(parseInt(map.coveragePageMap.get("annualPremium")))).equal(JSON.stringify(parseInt(annuallyPhysicalDamageValue)))
    
    })
})