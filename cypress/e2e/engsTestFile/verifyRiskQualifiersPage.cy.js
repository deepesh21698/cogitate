//import riskQualifierPage locators
import riskQualifierPageEngs from '../../pageObject/riskQualifierPage/riskQualifierPageEngs'

//import webdata
import webData from '../../fixtures/webData.json'

describe("Verification of login page", () => {
    it("verify login page", () => {
        cy.login(Cypress.env("username"),Cypress.env("password"))
       })

})
describe("Verification of accounthub page", () => {
    it("verify generate quote icon", () => {
        cy.generateQuoteIcon()
    })

})
describe("Verification of riskqualifiers page", () => {

    it("verify risk qualifers questions", () => {
     
        //verify question of engs
        riskQualifierPageEngs.getEngsQuestions().each((ele, index) => {
            cy.wrap(ele).should('be.visible')
            //convert text into uppercase
            const questionTxt = ele.text().toUpperCase()
            //assert the questions
            expect(questionTxt).to.contain(webData.application.cogitate.engs.riskQualifiersPage.questions[index])
        })
        //click on continue button
        riskQualifierPageEngs.getSubmitBtn().should('be.visible').and('contain.text', 'CONTINUE').click()
        
    })
    it("verify navigate to bussiness info page", () => {
        cy.url().should('include', 'businessinfo')

    })
})