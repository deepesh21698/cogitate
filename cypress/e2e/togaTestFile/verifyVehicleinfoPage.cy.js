//import bussiness info page
import vehicleinfoPage from '../../pageObject/vehicleInfoPage/vehicleinfoPageToga'
//import webdata
import webData from '../../fixtures/webData.json'
import testData from '../../fixtures/testData.json'
import { generateRandomNumber, getRandomNumber } from '../../support/commands'
import map from '../../support/commands'
import { faker } from '@faker-js/faker';
describe("Verification of login page", () => {
    it("verify login page", () => {
        cy.login(Cypress.env("username"), Cypress.env("password"))
    })

})
describe("Verification of accounthub page", () => {
    it("verify generate quote icon", () => {
        cy.generateQuoteIcon()
    })

})
describe("Verification of risk qualifier page", () => {
    it("verify riskqualifiers questions", () => {
        cy.togaQuestions()
    })
})
describe("Verification of bussinessinfo page", () => {
    it("verify bussinessinfo page detail", () => {
        cy.bussinessinfoDetails(

            webData.application.cogitate.toga.bussinessInfoPage.retailAgencyList[2],
            webData.application.cogitate.toga.bussinessInfoPage.underwriterList[generateRandomNumber(16)],
            faker.string.numeric(6),
            webData.application.cogitate.toga.bussinessInfoPage.insuredTypeList[generateRandomNumber(2)],
            faker.company.name({ length: 8 }),
            faker.word.sample({ length: 8 }),
            faker.word.words({ count: 6 }),
            webData.application.cogitate.toga.bussinessInfoPage.applicantSubsidiaryList[0],
            faker.phone.number(),
            webData.application.cogitate.toga.bussinessInfoPage.smsAlertList[getRandomNumber(4)],
            faker.phone.number(),
            testData.application.cogitate.toga.bussinessInfoPage.address,
            webData.application.cogitate.toga.bussinessInfoPage.commodityList[generateRandomNumber(123)],
            testData.application.cogitate.toga.bussinessInfoPage.hauledValue,
            testData.application.cogitate.toga.bussinessInfoPage.maximumValueFieldValue,
            testData.application.cogitate.toga.bussinessInfoPage.averageFieldValue,
            webData.application.cogitate.toga.bussinessInfoPage.operationList[generateRandomNumber(7)],
            faker.location.city(),
            webData.application.cogitate.toga.bussinessInfoPage.statesList[generateRandomNumber(11)],
            webData.application.cogitate.toga.bussinessInfoPage.radiusList[generateRandomNumber(3)],
            testData.application.cogitate.toga.bussinessInfoPage.percentageFieldValue
        )
    })
})
describe("Verification of vehicle info page", () => {
    it("verify vin field", () => {
        vehicleinfoPage.getVehicleVinInputField().then(($vin) => {
            //verify label 
            cy.wrap($vin).prev().should('be.visible')
                .and('contain.text', webData.application.cogitate.toga.vehicleinfoPage.vinFieldLabel)
            //verify field
            cy.wrap($vin).should('be.visible').and('be.enabled').and('be.empty')
                .type(testData.application.cogitate.toga.vehicleInfoPage.vehicleVinNum)
            //verify field is not blanck
            cy.wrap($vin).invoke('val').should('not.be.empty')
        })
        //verify fetch data button
        vehicleinfoPage.getFetchDataBtn().should('be.visible').and('be.enabled')
            .and('contain.text', webData.application.cogitate.toga.vehicleinfoPage.fetchBtnTxt)
    })
    it("verify override vin buttons", () => {
        vehicleinfoPage.getOverrideVinYesBtn().then(($yesBtn) => {
            //verify label
            cy.wrap($yesBtn).parents('.custToggle').prev()
                .should('be.visible').and('contain.text', webData.application.cogitate.toga.vehicleinfoPage.vinBtnsLabel)
            //verify text of button
            cy.wrap($yesBtn).parent().should('contain.text', 'Yes')
            //verify buttons
            cy.wrap($yesBtn).should('be.visible').and('be.enabled').check({ force: true })
            //verify yes button has clicked
            cy.wrap($yesBtn).parent().should('have.class', 'active')
            //verify no button
            cy.wrap($yesBtn).parent().next().find('input').then(($noBtn) => {
                //verify text of button
                cy.wrap($noBtn).parent().should('contain.text', 'No')
                //verify buttons
                cy.wrap($noBtn).should('be.visible').and('be.enabled').check({ force: true })
                //verify no button has clicked
                cy.wrap($noBtn).parent().should('have.class', 'active')
            })
            //click on yes button
            cy.wrap($yesBtn).click({ force: true })
        })
    })
    it("verify vehicle type dropdown", () => {

        //click on yes btn
        vehicleinfoPage.getVehicleTypeDropdown().invoke('css', 'opacity', 1).should('be.visible')
            .click({ force: true })
        //verify list
        vehicleinfoPage.getDropdownList().should('be.visible').each(($vehicleType, index) => {
            cy.wrap($vehicleType).should('contain.text', webData.application.cogitate.toga.vehicleinfoPage.vehicleTypeList[index])
        })
        //select the vehicle type
        let vehicleType = webData.application.cogitate.toga.vehicleinfoPage.vehicleTypeList[generateRandomNumber(7)]
        vehicleinfoPage.getDropdownList().each(($vehicleType) => {
            if ($vehicleType.text().includes("Truck-Tractor")) {
                cy.wrap($vehicleType).click({ force: true })
            }
        })
        //verify value has selected
        vehicleinfoPage.getSelectedVehicleType()
            .should('contain.text', "Truck-Tractor")

    })
    it("verify year dropdown", () => {

       vehicleinfoPage.getYearDropdown().should('be.visible')
            .click({ force: true })
        //select value
       vehicleinfoPage.getYearDropdownList().each(($year) => {
            if ($year.text().includes("2020")) {
                cy.wrap($year).click({ force: true })
            }
        })
    })
    it("verify make field", () => {
        //open make dropdown
       vehicleinfoPage.getMakeDropdown().should('be.visible').click({ force: true })
        //select value
       vehicleinfoPage.getMakeList().each(($make) => {
            if ($make.text().includes("Audi")) {
                cy.wrap($make).click({ force: true })
            }
        })
    })
    it("verify model name field", () => {
        vehicleinfoPage.getModelInputField().then(($model) => {
            //verify label
            cy.wrap($model).prev().should('be.visible')
                .and('contain.text', webData.application.cogitate.toga.vehicleinfoPage.modelFieldLabel)
            //verify field
            cy.wrap($model).should('be.visible').and('be.enabled').and('be.empty')
                .type(testData.application.cogitate.toga.vehicleInfoPage.modelFieldValue)
            //verify field is not empty
            cy.wrap($model).invoke('val').should('not.be.empty')
        })
    })
    it("verify stated value field", () => {
        vehicleinfoPage.getStatedValueField().then(($statedValue) => {
            //verify label
            cy.wrap($statedValue).parent().prev().should('be.visible')
                .and('contain.text', webData.application.cogitate.toga.vehicleinfoPage.statedValueFieldLabel)
            cy.wrap($statedValue).prev().should('contain.text', "$").and('be.visible')
            //verify field
            cy.wrap($statedValue).should('be.visible').and('be.enabled')
                .and('be.empty').type(testData.application.cogitate.toga.vehicleInfoPage.statedFieldValue, { force: true })
            //verify field is not empty
            cy.wrap($statedValue).invoke('val').should('not.be.empty')
        })

    })
    it("verify estimated mileage field", () => {
        vehicleinfoPage.getEstimatedValueField().then(($mileage) => {
            //verify label
            cy.wrap($mileage).prev().should('be.visible')
                .and('contain.text', webData.application.cogitate.toga.vehicleinfoPage.estimatedMileageFieldLabel)
            //verify field
            cy.wrap($mileage).invoke('css', 'z-index', 0).should('be.visible').and('be.enabled').and('be.empty')
                .type(testData.application.cogitate.toga.vehicleInfoPage.estimatedMileageFieldValue)
            //verify field is not empty
            cy.wrap($mileage).invoke('val').should('not.be.empty')
        })

    })
    it("verify garaging address", () => {
        cy.log(map.bussinesinfoTogaMap.get("garagingAddress"))
        vehicleinfoPage.getGaragingLocation().then(($garagingAddress) => {
            cy.log(map.bussinesinfoTogaMap.get("garagingAddress"))
            //verify label
            cy.wrap($garagingAddress).parents('.custSelect').prev()
                .should('be.visible').and('contain.text', webData.application.cogitate.toga.vehicleinfoPage.garagingAddressFieldLabel)
            //verify address
            cy.wrap($garagingAddress).should('contain.text', map.bussinesinfoTogaMap.get("garagingAddress"))

        })
    })
    it("verify ownership dropdown", () => {
        //open dropdown
        vehicleinfoPage.getOwnershipDropdown().should('be.visible').click({ force: true })
        //verify list
        vehicleinfoPage.getOwnershipList().each(($ownership, index) => {
            cy.wrap($ownership).should('contain.text', webData.application.cogitate.toga.vehicleinfoPage.vehicleOwnershipList[index])
        })
        //select the option
        let ownership = webData.application.cogitate.toga.vehicleinfoPage.vehicleOwnershipList[3]
        vehicleinfoPage.getOwnershipList().each(($ownership) => {
            if ($ownership.text().includes(ownership)) {
                cy.wrap($ownership).click({ force: true })
            }
        })
    })
    it("verify trailor type", () => {
        //open dropdown
        vehicleinfoPage.getTrailorTypeDropdown().should('be.visible').click({ force: true })
        cy.wait(2000)
        vehicleinfoPage.getTrailorTypeList().each(($ele) => {
            if ($ele.text().includes("Tanker Trailers")) {
                cy.wrap($ele).click({ force: true })
            }
        })
    })
    it("verify logging device button", () => {
        //select no logging
        vehicleinfoPage.getLoggingNoBtn().should('be.visible').click({ force: true })
        //node vice 
        vehicleinfoPage.getDetailField().should('be.visible').type(faker.lorem.words(7))
    })
    it("verify camera unit dropdown", () => {
        //camera unit
        vehicleinfoPage.getCameraUnitDropdown().should('be.visible').click({ force: true })
        vehicleinfoPage.getCameraUnitList().each(($ele) => {
            if ($ele.text().includes("No")) {
                cy.wrap($ele).click({ force: true })
            }
        })
    })
    it("verify footer buttons", () => {
        vehicleinfoPage.getSubmitBtn().should('be.visible').click({ force: true })
        cy.wait(3000)
        //click on next btn
        vehicleinfoPage.getLoader().should('not.exist')
    })
})