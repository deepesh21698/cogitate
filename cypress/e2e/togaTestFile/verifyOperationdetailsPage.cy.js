//import bussiness info page
import vehicleinfoPage from '../../pageObject/vehicleInfoPage/vehicleinfoPageToga'
//import webdata
import webData from '../../fixtures/webData.json'
import testData from '../../fixtures/testData.json'
import credentials from '../../fixtures/credentials.json'
import { generateRandomNumber, getRandomNumber } from '../../support/commands'
import map from '../../support/commands'
import { faker } from '@faker-js/faker';
describe("Verification of login page", () => {
    it("verify login page", () => {
        cy.login(Cypress.env("username"), Cypress.env("password"))
    })

})
describe("Verification of accounthub page", () => {
    it("verify generate quote icon", () => {
        cy.generateQuoteIcon()
    })

})
describe("Verification of risk qualifier page", () => {
    it("verify riskqualifiers questions", () => {
        cy.togaQuestions()
    })
})
describe("Verification of bussinessinfo page", () => {
    it("verify bussinessinfo page detail", () => {
        cy.bussinessinfoDetails(

            webData.application.cogitate.toga.bussinessInfoPage.retailAgencyList[2],
            webData.application.cogitate.toga.bussinessInfoPage.underwriterList[generateRandomNumber(16)],
            faker.string.numeric(6),
            webData.application.cogitate.toga.bussinessInfoPage.insuredTypeList[generateRandomNumber(2)],
            faker.company.name({ length: 8 }),
            faker.word.sample({ length: 8 }),
            faker.word.words({ count: 6 }),
            webData.application.cogitate.toga.bussinessInfoPage.applicantSubsidiaryList[0],
            faker.phone.number(),
            webData.application.cogitate.toga.bussinessInfoPage.smsAlertList[getRandomNumber(4)],
            faker.phone.number(),
            testData.application.cogitate.toga.bussinessInfoPage.address,
            webData.application.cogitate.toga.bussinessInfoPage.commodityList[generateRandomNumber(123)],
            testData.application.cogitate.toga.bussinessInfoPage.hauledValue,
            testData.application.cogitate.toga.bussinessInfoPage.maximumValueFieldValue,
            testData.application.cogitate.toga.bussinessInfoPage.averageFieldValue,
            webData.application.cogitate.toga.bussinessInfoPage.operationList[generateRandomNumber(7)],
            faker.location.city(),
            webData.application.cogitate.toga.bussinessInfoPage.statesList[generateRandomNumber(11)],
            webData.application.cogitate.toga.bussinessInfoPage.radiusList[generateRandomNumber(3)],
            testData.application.cogitate.toga.bussinessInfoPage.percentageFieldValue
        )
    })
})


describe("Verification of vehicle info page",()=>{
    it("verify vehicle details",()=>{
        cy.vehicleInfo(faker.string.binary({length:17}),"Truck-Tractor","2020",'Audi',faker.string.numeric({length:3}),faker.string.numeric({length:3}),faker.string.numeric({length:3}),"Non-Owned","Dry Van Trailers",faker.word.words({count:4}),"No")
    })
})

describe("Verification driver info page",()=>{
    it("verify driver details",()=>{
        cy.driverinfoPage(faker.person.firstName(),faker.person.lastName(),"Owner",faker.string.alphanumeric({length:8}),"AL",faker.phone.number(),'10','0','1')
    })
})

describe("Verification of operation details page",()=>{
    it("verify operation details",()=>{
        cy.operationdetailsPage("Auto Physical Damage (APD)","2021",faker.number.int(),faker.company.name(),"10000","2000","5000")
    })
})