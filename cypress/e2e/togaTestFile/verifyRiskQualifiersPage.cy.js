//import riskQualifierPage locators
import riskQualifierPageToga from '../../pageObject/riskQualifierPage/riskQualifierPageToga'

//import webdata
import webData from '../../fixtures/webData.json'
import credentials from '../../fixtures/credentials.json'
describe("Verification of login page", () => {
    it("verify login page", () => {
        cy.login(Cypress.env("username"), Cypress.env("password"))
    })

})
describe("Verification of accounthub page", () => {
    it("verify generate quote icon", () => {
        cy.generateQuoteIcon()
    })

})
describe("Verification of riskqualifiers page", () => {

    it("verify risk qualifers questions", () => {
     
        //verify question of toga
        riskQualifierPageToga.getTogaQuestions().each((ele, index) => {
            cy.wrap(ele).should('be.visible')
            //store questions text
            const questionTxt = ele.text()
            //assert questions
            expect(questionTxt).to.contain(webData.application.cogitate.toga.riskQualifiersPage.questions[index])
        })
        cy.xpath("//*[text()='No']").click({ multiple: true, force: true })
        cy.xpath("//*[text()='Agree']").click({ multiple: true, force: true })
        cy.get('#successBTN').click()
        //click on next btn
        riskQualifierPageToga.getSubmitBtn().should('be.visible').and('contain.text', 'Next').click()


    })
    it("verify navigate to bussiness info page", () => {
        cy.url().should('include', 'businessinfo')

    })
})