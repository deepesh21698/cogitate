//import bussiness info page
import bussinessInfoPage from '../../pageObject/bussinessinfoPage/bussinessinfoPageToga'
//import webdata
import webData from '../../fixtures/webData.json'
import testData from '../../fixtures/testData.json'
import { generateRandomNumber, getRandomNumber } from '../../support/commands'
import { email } from '../../support/commands'
import { getCurrentMonthText } from '../../support/commands'
var bussinessInfoTogaMap = new Map()
describe("Verification of login page", () => {
    it("verify login page", () => {
        cy.login(Cypress.env("username"), Cypress.env("password"))
    })

})
describe("Verification of accounthub page", () => {
    it("verify generate quote icon", () => {
        cy.generateQuoteIcon()
    })

})
describe("Verification of risk qualifier page", () => {
    it("verify riskqualifiers questions", () => {
        cy.togaQuestions()
    })

})
describe("Verification of bussiness info page", () => {
    it("verify agent and underwriter fields ", () => {
        context("verify retail agency dropdown", () => {
            //open retail agency dropdown
            bussinessInfoPage.getRetailAgencyDropdown().scrollIntoView().should('be.visible').then(($retailAgency) => {
                cy.wrap($retailAgency).click({ force: true })
            })
            bussinessInfoPage.getDropdownList().should('be.visible').then(($agencyList) => {
                cy.wrap($agencyList).each((ele, index) => {
                    cy.wrap(ele).should('contain.text', webData.application.cogitate.toga.bussinessInfoPage.retailAgencyList[index])
                })
                //select retail agency
                //retail list
                bussinessInfoPage.getDropdownList().each(($retail) => {
                    if ($retail.text().includes("Allegiance Insurance Services, LLC (AGT018)")) {
                        cy.wrap($retail).click({ force: true })
                    }
                })
                cy.wait(3000)
                //verify option has selected
                bussinessInfoPage.getRetailAgencyDropdown().scrollIntoView().parent().prev().invoke('text')
                    .then((agency) => {
                        expect(agency).equal(webData.application.cogitate.toga.bussinessInfoPage.retailAgencyList[2])
                    })
            })

        })

        context("verify agent name dropdown", () => {
            //open agent name dropdown
            bussinessInfoPage.getAgentNameDropdown().scrollIntoView().click({ force: true })
            bussinessInfoPage.getDropdownList().then(($agentNameList) => {
                //verify agent name dropdown
                cy.wrap($agentNameList).each(($ele) => {
                    if ($ele.text().includes("Allegiance Insurance Services, LLC (214426)")) {
                        cy.wrap($ele).click({ force: true })
                    }
                })

            })

        })

        context("verify underwriter field", () => {
            //open underwriter dropdown
            bussinessInfoPage.getUnderwriterDropdown().as("underwriter").should('be.visible').click({ force: true })
            //verify dropdowin list
            bussinessInfoPage.getDropdownList().should('be.visible').each(($ele, index) => {
                cy.wrap($ele).should('contain.text', webData.application.cogitate.toga.bussinessInfoPage.underwriterList[index])
            })
            let underwriter = webData.application.cogitate.toga.bussinessInfoPage.underwriterList[generateRandomNumber(10)]
            //select the option
            bussinessInfoPage.getDropdownList().should('be.visible').each(($ele) => {
                if ($ele.text().includes(underwriter)) {
                    cy.wrap($ele).click({ force: true })

                }
            })
            cy.wait(3000)
            //verify option has selected
            cy.get("@underwriter").parent().prev().should('be.visible').invoke('text')
                .then((actualUnderwriterName) => {
                    expect(actualUnderwriterName).equal(underwriter)
                })
        })
    })
    it("verify bussiness details fields", () => {
        context("verify us dot input field", () => {
            bussinessInfoPage.getUsdotInputField().then(($dotField) => {
                //verify label of us dot field
                cy.wrap($dotField).prev().should('contain.text', webData.application.cogitate.toga.bussinessInfoPage.dotFieldLabel)

                cy.wrap($dotField).scrollIntoView().should('be.visible')
                    .and('be.enabled').and('be.empty').type(testData.application.cogitate.toga.bussinessInfoPage.usDotNumber, { force: true })
                //verify field is not empty
                cy.wrap($dotField).invoke('val').should('not.be.empty')
                //verify us dot search button
                bussinessInfoPage.getDotSearchBtn().should('be.visible')
                    .and('be.enabled').and('contain.text', webData.application.cogitate.toga.bussinessInfoPage.searchBtnTxt)
            })
        })
        context("verify insured type dropdown", () => {

            let expectdeInsuredType
            //verify insured dropdown
            bussinessInfoPage.getInsuredTypeDropdown().scrollIntoView().as("insuredType").should('be.visible').click({ force: true })
            //verify insured type dropdown list
            bussinessInfoPage.getInsuredTypeList().should('be.visible').each(($ele, index) => {
                cy.wrap($ele).should('be.visible').and('contain.text', webData.application.cogitate.toga.bussinessInfoPage.insuredTypeList[index])
            })
            expectdeInsuredType = webData.application.cogitate.toga.bussinessInfoPage.insuredTypeList[generateRandomNumber(3)]
            //select the insured type option
            bussinessInfoPage.getInsuredTypeList().each(($ele) => {
                if ($ele.text().includes(expectdeInsuredType)) {

                    cy.wrap($ele).click({ force: true })
                    cy.wait(3000)
                }
            })

            //verify option has selected as expected
            cy.get("@insuredType").parent().prev().scrollIntoView().then(($insuredType) => {
                cy.wrap($insuredType).should('contain.text', expectdeInsuredType)
                //store value in map
                bussinessInfoTogaMap.set("insuredType", $insuredType.text())

                if (bussinessInfoTogaMap.get("insuredType") === 'Individual') {
                    context("verify name fields", () => {
                        //verify first name fields
                        bussinessInfoPage.getFirstNameField().then(($firstNameField) => {

                            //verify firstname label 
                            cy.wrap($firstNameField).prev().should('be.visible').and('contain.text', webData.application.cogitate.toga.bussinessInfoPage.firstNameFieldLabel)
                            cy.wrap($firstNameField).should('be.visible').and('be.enabled')
                                .and('be.empty').type(testData.application.cogitate.toga.bussinessInfoPage.firstName, { force: true })
                            //verify first name field is not empty
                            cy.wrap($firstNameField).invoke('val').should('not.be.empty')
                        })
                        //verify middle name fields
                        bussinessInfoPage.getMiddleNameField().then(($middleNameField) => {
                            //verify middle name label 
                            cy.wrap($middleNameField).prev().should('be.visible').and('contain.text', webData.application.cogitate.toga.bussinessInfoPage.middleNameFieldLabel)
                            cy.wrap($middleNameField).should('be.visible').and('be.enabled')
                                .and('be.empty').type(testData.application.cogitate.toga.bussinessInfoPage.middleName, { force: true })
                            //verify middle name field is not empty
                            cy.wrap($middleNameField).invoke('val').should('not.be.empty')
                        })
                        //verify last name fields
                        bussinessInfoPage.getLastNameField().then(($lastNameField) => {
                            //verify last name label 
                            cy.wrap($lastNameField).prev().should('be.visible').and('contain.text', webData.application.cogitate.toga.bussinessInfoPage.lastNameLabel)
                            cy.wrap($lastNameField).should('be.visible').and('be.enabled')
                                .and('be.empty').type(testData.application.cogitate.toga.bussinessInfoPage.lastName, { force: true })
                            //verify last name field is not empty
                            cy.wrap($lastNameField).invoke('val').should('not.be.empty')
                        })
                    })
                } else {
                    context("verify bussiness name input field", () => {
                        bussinessInfoPage.getBussinessNameField().then(($bussinessNameField) => {
                            //verify bussiness name field label
                            cy.wrap($bussinessNameField).prev().should('contain.text', webData.application.cogitate.toga.bussinessInfoPage.bussinessNameFieldLabel)
                            //verify field is visible and enabled
                            cy.wrap($bussinessNameField).should('be.visible').and('be.enabled').and('be.empty')
                                .type(testData.application.cogitate.toga.bussinessInfoPage.bussinessNameValue, { force: true })
                            //verify field is not empty
                            cy.wrap($bussinessNameField).invoke('val').should('not.be.empty')
                        })
                    })
                }

            })

        })


        context("verify DBA input field", () => {
            bussinessInfoPage.getDbaName().then(($dbaName) => {
                //verify DBA label
                cy.wrap($dbaName).prev().should('be.visible').and('contain.text', webData.application.cogitate.toga.bussinessInfoPage.dbaLabel)
                //verify field
                cy.wrap($dbaName).should('be.visible').and('be.enabled').and('be.empty')
                    .type(testData.application.cogitate.toga.bussinessInfoPage.dbaValue, { force: true })
                //verify field is not empty
                cy.wrap($dbaName).invoke('val').should('not.be.empty')
            })
        })
        context("verify bussiness description field", () => {
            bussinessInfoPage.getBussinessDescriptionField().then(($bussinessDescriptionField) => {
                //verify DBA label
                cy.wrap($bussinessDescriptionField).prev().should('be.visible').and('contain.text', webData.application.cogitate.toga.bussinessInfoPage.bussinessDecriptionLabel)
                //verify field
                cy.wrap($bussinessDescriptionField).should('be.visible').and('be.enabled').and('be.empty')
                    .type(testData.application.cogitate.toga.bussinessInfoPage.bussinessDecription, { force: true })
                //verify field is not empty
                cy.wrap($bussinessDescriptionField).invoke('val').should('not.be.empty')
            })
        })

        context("verify subsidiary dropdown", () => {
            //open subsidiary dropdown
            bussinessInfoPage.getApplicantSubsidiaryDropdown().as("subsidiary").click({ force: true })
            //verify list
            bussinessInfoPage.getApplicantSubsidiaryList().each(($ele, index) => {
                cy.wrap($ele).should('contain.text', webData.application.cogitate.toga.bussinessInfoPage.applicantSubsidiaryList[index]).and('be.visible')
            })
            let subsidiary = webData.application.cogitate.toga.bussinessInfoPage.applicantSubsidiaryList[0]
            bussinessInfoPage.getApplicantSubsidiaryList().each(($ele) => {
                if ($ele.text().includes(subsidiary)) {
                    cy.wrap($ele).click({ force: true })
                }
            })
            cy.wait(3000)
            //verify option has selected as expected
            cy.get("@subsidiary").parent().prev().should('be.visible').then(($subsidiary) => {
                cy.wrap($subsidiary).should('contain.text', subsidiary)
            })
        })
      
        context("verify effective date coverage field", () => {
            let date = new Date()
            let month = date.getMonth() + 1
            let day = date.getDate()
            bussinessInfoPage.getEffectiveDateField().should('be.visible').then(($date) => {
                //verify label of effective date field
                cy.wrap($date).prev().should('contain.text', webData.application.cogitate.toga.bussinessInfoPage.effectiveDateFieldLabel).and('be.visible')
                //open calender
                cy.wrap($date).find('input').eq(1).click({ force: true })
                cy.wait(2000)
                bussinessInfoPage.getCalender().first().click({ force: true })
                cy.wait(2000)
                cy.contains(getCurrentMonthText(month)).click({ force: true })
                cy.wait(2000)
                cy.contains(day).click({ force: true })
                //get month
                cy.wrap($date).find('input').eq(1).invoke('val').then((month) => {
                    cy.log(month)
                })
                //get date
                cy.wrap($date).find('input').eq(2).invoke('val').then((date) => {
                    cy.log(date)
                })
                //get year
                cy.wrap($date).find('input').eq(3).invoke('val').then((year) => {
                    cy.log(year)
                })
            })
        })

        context("verify policy period dropdown", () => {
            bussinessInfoPage.getPolicyPeriodDropdown().then(($policyDropdown) => {
                //verify label of policy period dropdown
                cy.wrap($policyDropdown).prev().should('contain.text', webData.application.cogitate.toga.bussinessInfoPage.policyPeriodDropdownLabel)
                //verify policy period dropdown is disabled
                cy.wrap($policyDropdown).should('be.visible').and('be.disabled')
                    //verify value is 12
                    .find('option').should('have.attr', 'value', '12')
            })
        })
        context("verify current insurance policy information", () => {

            bussinessInfoPage.getCurrentPolicyInfoBtns().then(($currentInsuranceBtns) => {
                //verify label
                cy.wrap($currentInsuranceBtns).parent().parent().prev().should('be.visible')
                    .and('contain.text', webData.application.cogitate.toga.bussinessInfoPage.currentInsurancePolicyInfoLabel)
                //verify yes btn
                cy.wrap($currentInsuranceBtns).first().as('yesBtn').should('be.visible').and('contain.text', "Yes")
                    .find('input').should('be.enabled').click({ force: true })
                //verify yes btn has clicked
                cy.get("@yesBtn").should('have.class', 'active')
                //verify no btn
                cy.wrap($currentInsuranceBtns).first().next().as('noBtn').should('be.visible').and('contain.text', "No")
                    .find('input').should('be.enabled').click({ force: true })
                //verify no btn has clicked
                cy.get("@noBtn").should('have.class', 'active')
                //verify yes btn should not be checked when click on no btn
                cy.get("@yesBtn").should('not.have.class', 'active')
            })
        })
        context("verify insured mid term buttons", () => {

            bussinessInfoPage.getInsuredMidTermBtns().then(($midTermBtns) => {
                //verify label
                cy.wrap($midTermBtns).first().parent().parent().prev().should('be.visible')
                    .and('contain.text', "Is insured looking to move mid-term?*")
                //verify yes btn
                cy.wrap($midTermBtns).first().as('yesBtn').should('be.visible').and('contain.text', "Yes")
                    .find('input').should('be.enabled').click({ force: true })
                //verify yes btn has clicked
                cy.get("@yesBtn").should('have.class', 'active')
                //verify no btn
                cy.wrap($midTermBtns).first().next().as('noBtn').should('be.visible').and('contain.text', "No")
                    .find('input').should('be.enabled').click({ force: true })
                //verify no btn has clicked
                cy.get("@noBtn").should('have.class', 'active')
                //verify yes btn should not be checked when click on no btn
                cy.get("@yesBtn").should('not.have.class', 'active')
            })
        })
        context("verify policy decline btns", () => {
            bussinessInfoPage.getPolicyDeclineBtns().then(($policyDecline) => {
                //verify label
                cy.wrap($policyDecline).parent().parent().prev().should('be.visible')
                    .and('contain.text', webData.application.cogitate.toga.bussinessInfoPage.policyDeclineLabel)
                // verify yes btn
                cy.wrap($policyDecline).first().as('yesBtn').should('be.visible').and('contain.text', "Yes")
                    .find('input').should('be.enabled').click({ force: true })
                //verify yes btn has clicked
                cy.get("@yesBtn").should('have.class', 'active')
                //verify no btn
                cy.wrap($policyDecline).first().next().as('noBtn').should('be.visible').and('contain.text', "No")
                    .find('input').should('be.enabled').click({ force: true })
                //verify no btn has clicked
                cy.get("@noBtn").should('have.class', 'active')
                //verify yes btn should not be checked when click on no btn
                cy.get("@yesBtn").should('not.have.class', 'active')
            })
        })
        context("verify motor carrier field", () => {
            bussinessInfoPage.getMotorCarrierField().then(($motorCarrier) => {
                //verify label
                cy.wrap($motorCarrier).prev().should('be.visible').and('contain.text', webData.application.cogitate.toga.bussinessInfoPage.motorCarrier)
                //verify input field
                cy.wrap($motorCarrier).should('be.visible').and('be.enabled').and('be.empty')
                    .type(testData.application.cogitate.toga.bussinessInfoPage.motorCarrierValue, { force: true })
                //verify field is not empty
                cy.wrap($motorCarrier).invoke('val').should('not.be.empty')
            })
        })

        let startedYear
        context("verify bussiness started date field", () => {
            
            bussinessInfoPage.getBussinessStartedDateField().should('be.visible').find('input')
                .eq(1).type("003", { force: true })
            bussinessInfoPage.getBussinessStartedDateField().find('input')
                .eq(2).type("2010", { force: true }).invoke('val').then((year) => {
                    startedYear = year
                })
        })

        context("verify years in bussiness field", () => {
            bussinessInfoPage.getYearInBussinessField().then(($yearInBussiness) => {
                //verify label
                cy.wrap($yearInBussiness).prev().should('contain.text', webData.application.cogitate.toga.bussinessInfoPage.yearInBussinessLabel)
                cy.wrap($yearInBussiness).should('be.visible').and('be.disabled')
                //verify value
                cy.log(startedYear)
                let date = new Date()
                let currentYear = date.getFullYear()
                let yearsInBussinessValue = currentYear - startedYear
                cy.log(yearsInBussinessValue)
                //verify years in bussiness value
                cy.wrap($yearInBussiness).should('have.attr', 'value', yearsInBussinessValue)
            })
        })
        context("verify ownership changed btns", () => {
            bussinessInfoPage.getOwnershipChangedBtns().then(($ownershipBtns) => {
                //verify label
                cy.wrap($ownershipBtns).parent().parent().prev().should('be.visible')
                    .and('contain.text', webData.application.cogitate.toga.bussinessInfoPage.ownershipChangedLabel)
                // verify yes btn
                cy.wrap($ownershipBtns).first().as('yesBtn').should('be.visible').and('contain.text', "Yes")
                    .find('input').should('be.enabled').click({ force: true })
                //verify yes btn has clicked
                cy.get("@yesBtn").should('have.class', 'active')
                //verify no btn
                cy.wrap($ownershipBtns).first().next().as('noBtn').should('be.visible').and('contain.text', "No")
                    .find('input').should('be.enabled').click({ force: true })
                //verify no btn has clicked
                cy.get("@noBtn").should('have.class', 'active')
                //verify yes btn should not be checked when click on no btn
                cy.get("@yesBtn").should('not.have.class', 'active')
            })
        })
        context("verify sms alerts dropdown", () => {
            bussinessInfoPage.getSmsAlertDropdown().as("smsDropdown").then(($smsAlert) => {
                //verify label
                cy.wrap($smsAlert).should('be.visible').click({ force: true })

                //verify sms alert list
                bussinessInfoPage.getSmsAlertList().each(($ele, index) => {
                    cy.wrap($ele.text()).should('contain', webData.application.cogitate.toga.bussinessInfoPage.smsAlertList[index])
                })
                let alertValue = webData.application.cogitate.toga.bussinessInfoPage.smsAlertList[getRandomNumber(5)]
                //select the value
                bussinessInfoPage.getSmsAlertList().each(($ele) => {
                    if ($ele.text().includes(alertValue)) {
                        cy.wrap($ele).click({ force: true })
                    }

                })
                //verify value has selected as expected
                cy.get("@smsDropdown").parent().prev().should('contain.text', alertValue)
            })
        })
    })
    it("verify contact information fields", () => {
        context("verify first name field", () => {
            //verify first name fields
            bussinessInfoPage.getContactInfoFirstNameField().then(($firstNameField) => {

                //verify firstname label 
                cy.wrap($firstNameField).prev().should('be.visible').and('contain.text', webData.application.cogitate.toga.bussinessInfoPage.firstNameFieldLabel)
                cy.wrap($firstNameField).should('be.visible').and('be.enabled')
                    .and('be.empty').type(testData.application.cogitate.toga.bussinessInfoPage.firstName, { multiple: true, force: true })
                //verify first name field is not empty
                cy.wrap($firstNameField).invoke('val').should('not.be.empty')
            })
        })
        context("verify middle name field", () => {
            //verify middle name fields
            bussinessInfoPage.getContactInfoMiddleNameField().then(($middleNameField) => {
                //verify middle name label 
                cy.wrap($middleNameField).prev().should('be.visible').and('contain.text', webData.application.cogitate.toga.bussinessInfoPage.middleNameFieldLabel)
                cy.wrap($middleNameField).should('be.visible').and('be.enabled')
                    .and('be.empty').type(testData.application.cogitate.toga.bussinessInfoPage.middleName, { force: true })
                //verify middle name field is not empty
                cy.wrap($middleNameField).invoke('val').should('not.be.empty')
            })
        })
        context("verify last name field", () => {
            //verify last name fields
            bussinessInfoPage.getContactInfoLastNameField().then(($lastNameField) => {
                //verify last name label 
                cy.wrap($lastNameField).prev().should('be.visible').and('contain.text', webData.application.cogitate.toga.bussinessInfoPage.lastNameLabel)
                cy.wrap($lastNameField).should('be.visible').and('be.enabled')
                    .and('be.empty').type(testData.application.cogitate.toga.bussinessInfoPage.lastName, { force: true })
                //verify last name field is not empty
                cy.wrap($lastNameField).invoke('val').should('not.be.empty')
            })
        })
        context("verify email field", () => {
            bussinessInfoPage.getEmailField().then(($emailField) => {
                //verify label
                cy.wrap($emailField).prev().should('contain.text', webData.application.cogitate.toga.bussinessInfoPage.emailFieldLabel)
                cy.wrap($emailField).should('be.visible').and('be.enabled')
                    .and('be.empty').type(email(), { force: true })
                //verify field is not empty
                cy.wrap($emailField).invoke('val').should('not.be.empty')
            })
        })
        context("verify phone field", () => {
            bussinessInfoPage.getPhoneField().then(($phoneField) => {
                //verify label
                cy.wrap($phoneField).prev().should('contain.text', webData.application.cogitate.toga.bussinessInfoPage.phoneFieldLabel)
                cy.wrap($phoneField).should('be.visible').and('be.enabled')
                    .and('be.empty').type(testData.application.cogitate.toga.bussinessInfoPage.phoneNumber, { force: true })
                //verify field is not empty
                cy.wrap($phoneField).invoke('val').should('not.be.empty')
            })
        })
    })
    it("verify location fields", () => {
        context("verify address field", () => {
            bussinessInfoPage.getLocationField().then(($adressField) => {
                //verify location field
                cy.wrap($adressField).should('be.visible').and('be.enabled').and('be.empty')
                    .type(testData.application.cogitate.toga.bussinessInfoPage.address, { force: true })
                    .invoke('val').then((address) => {
                        bussinessInfoTogaMap.set("address", address)
                    })
                cy.wait(2000)
                cy.wrap($adressField).type("{enter}")
            })
        })
        context("verify add address manually", () => {
            bussinessInfoPage.getAddAddressManuallySwitchBtn().then(($addressSwitchBtn) => {
                $addressSwitchBtn.on('click', () => {
                    cy.log("ssss clicked")
                })
                //verify label
                cy.wrap($addressSwitchBtn).prev()
                    .should('contain.text', webData.application.cogitate.toga.bussinessInfoPage.addManuallySwitchBtnLabel)
            })
        })
        let isMailingChecked = false
        //verify checkboxes 
        context("verify mailing checkbox", () => {
            //verify mailing address check box
            bussinessInfoPage.getMailingAddressCheckbox().invoke('css', 'opacity', '1').should('be.visible').then(($mailingAddressCheckbox) => {
                //verify label 
                cy.wrap($mailingAddressCheckbox).next().should("be.visible")
                    .and("contain.text", webData.application.cogitate.toga.bussinessInfoPage.mailingAddressCheckboxLabel)

                //verify checkbox functionality
                cy.wrap($mailingAddressCheckbox).should('not.be.checked')
                cy.wait(2000)
                cy.wrap($mailingAddressCheckbox).check({ force: true })
                cy.wrap($mailingAddressCheckbox).should('be.checked')
                cy.wait(2000)
                cy.wrap($mailingAddressCheckbox).uncheck({ force: true })
                cy.wrap($mailingAddressCheckbox).should('not.be.checked')
                if ($mailingAddressCheckbox.is(":checked")) {
                    isMailingChecked = true
                }

            })
        })
        let isGaragingChecked = false
        //verify checkboxes 
        context("verify garaging checkbox", () => {
            //verify mailing address check box
            bussinessInfoPage.getGaragingAddressCheckbox().invoke('css', 'opacity', '1').should('be.visible').then(($garagingAddressCheckbox) => {

                //verify label 
                cy.wrap($garagingAddressCheckbox).next().should("be.visible")
                    .and("contain.text", webData.application.cogitate.toga.bussinessInfoPage.garagingAddressCheckboxLabel)

                //verify checkbox functionality
                cy.wrap($garagingAddressCheckbox).should('not.be.checked')
                cy.wait(2000)
                cy.wrap($garagingAddressCheckbox).check({ force: true })
                cy.wrap($garagingAddressCheckbox).should('be.checked')
                cy.wait(2000)
                cy.wrap($garagingAddressCheckbox).uncheck({ force: true })
                cy.wrap($garagingAddressCheckbox).should('not.be.checked')

                if ($garagingAddressCheckbox.is(":checked")) {
                    isGaragingChecked = true
                }

            })
        })
        context("verify save button", () => {
            //verify save btn
            bussinessInfoPage.getSaveBtn().then(($saveBtn) => {

                bussinessInfoPage.getMailingAddressCheckbox().check({ force: true })
                bussinessInfoPage.getGaragingAddressCheckbox().check({ force: true })
                cy.wrap($saveBtn).should('be.visible').and('be.enabled')
                    .and('contain.text', "SAVE").click({ force: true })
                //verify address should be saved
                bussinessInfoPage.getSavedAddresses().should('have.length', 2)
            })

        })
    })

    it("verify commodities fields", () => {
        context("verify commodity dropdown", () => {
            let commodity
            //open commodity dropdown
            bussinessInfoPage.getCommoditityDropdown().should('be.visible').click({ force: true })
            //verify commodity list
            bussinessInfoPage.getCommodityList().should('be.visible').then(($commodity) => {
                cy.wrap($commodity).each(($commodity, index) => {
                    cy.wrap($commodity).should('contain.text', webData.application.cogitate.toga.bussinessInfoPage.commodityList[index])
                })

                commodity = webData.application.cogitate.toga.bussinessInfoPage.commodityList[generateRandomNumber(123)]
                cy.wrap($commodity).each(($commodity) => {
                    if ($commodity.text().includes(commodity)) {
                        cy.wrap($commodity).click({ force: true })
                    }
                })
            })
            cy.wait(3000)
            //verify option has selected as expected
            bussinessInfoPage.getCommoditityDropdown().parent().prev().then(($selectedValue) => {
                cy.wrap($selectedValue).should('contain.text', commodity).and('be.visible')
            })
        })
        context("verify hauled field", () => {
            bussinessInfoPage.getHauledField().then(($hauledField) => {
                //verify label
                cy.wrap($hauledField).parent().prev().should('be.visible')
                    .and('contain.text', webData.application.cogitate.toga.bussinessInfoPage.hauledFieldLabel)
                cy.wrap($hauledField).prev().should('contain.text', "%").and('be.visible')
                //verify field
                cy.wrap($hauledField).should('be.visible').and('be.enabled')
                    .and('be.empty').type(testData.application.cogitate.toga.bussinessInfoPage.hauledValue, { force: true })
                //verify field is not empty
                cy.wrap($hauledField).invoke('val').should('not.be.empty')
            })
        })
        context("verify maximum  value field field", () => {
            bussinessInfoPage.getMaximumValueField().then(($hauledField) => {
                //verify label
                cy.wrap($hauledField).parent().prev().should('be.visible')
                    .and('contain.text', webData.application.cogitate.toga.bussinessInfoPage.maximumValueFieldLabel)
                cy.wrap($hauledField).prev().should('contain.text', "$").and('be.visible')
                //verify field
                cy.wrap($hauledField).should('be.visible').and('be.enabled')
                    .and('be.empty').type(testData.application.cogitate.toga.bussinessInfoPage.maximumValueFieldValue, { force: true })
                //verify field is not empty
                cy.wrap($hauledField).invoke('val').should('not.be.empty')
            })
        })
        context("verify average value field", () => {
            bussinessInfoPage.getAverageValueField().then(($hauledField) => {
                //verify label
                cy.wrap($hauledField).parent().prev().should('be.visible')
                    .and('contain.text', webData.application.cogitate.toga.bussinessInfoPage.averageValueFieldLabel)
                cy.wrap($hauledField).prev().should('contain.text', "$").and('be.visible')
                //verify field
                cy.wrap($hauledField).should('be.visible').and('be.enabled')
                    .and('be.empty').type(testData.application.cogitate.toga.bussinessInfoPage.averageFieldValue, { force: true })
                //verify field is not empty
                cy.wrap($hauledField).invoke('val').should('not.be.empty')
            })
        })
    })
    it("verify additional operational details", () => {
        context("verify travelling out of state buttons", () => {
            bussinessInfoPage.getTravellingOutStateYesBtn().then(($yesBtn) => {
                //verify label
                cy.wrap($yesBtn).parent().parent().parent()
                    .prev().should('be.visible').and('contain.text', webData.application.cogitate.toga.bussinessInfoPage.travellingOutStateBtnsLabel)

                //verify btn text
                cy.wrap($yesBtn).parent().should('be.visible').and('contain.text', 'Yes')
                //verify yes button
                cy.wrap($yesBtn).should('be.visible').and('be.enabled').click({ force: true })
                //verify yes btn has clicked
                cy.wrap($yesBtn).parent().should('have.class', 'active')

                //verify no btn
                bussinessInfoPage.getTravellingOutStateNoBtn().then(($noBtn) => {
                    //verify btn text
                    cy.wrap($noBtn).parent().should('be.visible').and('contain.text', 'No')
                    //verify yes button
                    cy.wrap($noBtn).should('be.visible').and('be.enabled').click({ force: true })
                    //verify yes btn has clicked
                    cy.wrap($noBtn).parent().should('have.class', 'active')
                    //verify yes has not clicked
                    cy.wrap($yesBtn).parent().should('not.have.class', 'active')
                })

            })
        })
        context("verify type of operation dropdown", () => {
            //open the dropdown
            bussinessInfoPage.getTypeOperationDropdown().should('be.visible').click({ force: true })
            //verify list
            bussinessInfoPage.getTypeOperationList().should('be.visible').each(($operation, index) => {
                cy.wrap($operation).should('contain.text', webData.application.cogitate.toga.bussinessInfoPage.operationList[index])
            })
            //select the option
            let operationType = webData.application.cogitate.toga.bussinessInfoPage.operationList[generateRandomNumber(11)]
            bussinessInfoPage.getTypeOperationList().should('be.visible').each(($operation, index) => {
                if ($operation.text().includes(operationType)) {
                    cy.wrap($operation).click({ force: true })
                }
            })
            //verify expected option has selected
            bussinessInfoPage.getTypeOperationDropdown().parent().prev().should('contain.text', operationType)
        })
        context("verify major cities travelled field", () => {
            bussinessInfoPage.getMajorCitiesTravelledField().then(($majorCities) => {
                //verify label
                cy.wrap($majorCities).prev().should('be.visible').and('contain.text', webData.application.cogitate.toga.bussinessInfoPage.majorCitiesTravelledFieldLabel)
                //verify field
                cy.wrap($majorCities).should('be.visible').and('be.enabled').and('be.empty')
                    .type(testData.application.cogitate.toga.bussinessInfoPage.majorCityFieldValue, { force: true })

                //verify field is not empty
                cy.wrap($majorCities).invoke('val').should('not.be.empty')
            })
            context("verify states dropdown", () => {
                //open the dropdown
                bussinessInfoPage.getStatesDropdown().should('be.visible').click({ force: true })
                //verify list
                bussinessInfoPage.getStatesList().should('be.visible').each(($sates, index) => {
                    cy.wrap($sates).should('contain.text', webData.application.cogitate.toga.bussinessInfoPage.statesList[index])
                })
                //select the option
                let state = webData.application.cogitate.toga.bussinessInfoPage.statesList[generateRandomNumber(11)]
                bussinessInfoPage.getStatesList().should('be.visible').each(($sates) => {
                    if ($sates.text().includes(state)) {
                        cy.wrap($sates).click({ force: true })
                    }
                })
                //verify expected option has selected
                bussinessInfoPage.getStatesDropdown().parent().prev().should('contain.text', state).and('be.visible')
            })
        })
    })
    it("verify radius of operation fields", () => {
        context("verify radius dropdown", () => {
            //open dropdown
            bussinessInfoPage.getRadiusOperationDropdown().should('be.visible').click({ force: true })
            //verify list
            bussinessInfoPage.getRadiusOperationList().should('be.visible').each(($radius, index) => {
                cy.wrap($radius).should('contain.text', webData.application.cogitate.toga.bussinessInfoPage.radiusList[index])
            })
            //select the option
            let radius = webData.application.cogitate.toga.bussinessInfoPage.radiusList[generateRandomNumber(6)]
            bussinessInfoPage.getRadiusOperationList().each(($radius) => {
                if ($radius.text().includes(radius)) {
                    cy.wrap($radius).click({ force: true })
                }
            })
            //verify option has selected
            bussinessInfoPage.getRadiusOperationDropdown().parent().prev().invoke('css','opacity',1).should('be.visible')
                .and('contain.text', radius)

        })
        context("verify percentage field", () => {
            bussinessInfoPage.getPercentageField().then(($hauledField) => {
                //verify label
                cy.wrap($hauledField).parent().prev().should('be.visible')
                    .and('contain.text', webData.application.cogitate.toga.bussinessInfoPage.percentageFieldLabel)
                cy.wrap($hauledField).prev().should('contain.text', "%").and('be.visible')
                //verify field
                cy.wrap($hauledField).should('be.visible').and('be.enabled')
                    .and('be.empty').type(testData.application.cogitate.toga.bussinessInfoPage.percentageFieldValue, { force: true })
                //verify field is not empty
                cy.wrap($hauledField).invoke('val').should('not.be.empty')
            })
        })
    })
    it("verify navigation button", () => {
        //verify back button
        bussinessInfoPage.getBackBtn().should('be.visible').and('be.enabled').and('contain.text', webData.application.cogitate.toga.bussinessInfoPage.backBtnTxt)
        //verify save for latter btn
        bussinessInfoPage.getSaveForLaterBtn().should('be.visible').and('be.enabled').and('contain.text', webData.application.cogitate.toga.bussinessInfoPage.saveForLaterBtnTxt)
        //verify next button
        bussinessInfoPage.getContinueBtn().should('be.visible').and('be.enabled').and('contain.text', webData.application.cogitate.toga.bussinessInfoPage.nxtBtnTxt).click({ force: true })
    })

    it("verify navigate to vehicleinfo page", () => {
        bussinessInfoPage.getLoader().should('not.exist')
        cy.url().should('equal', webData.application.cogitate.toga.vehicleinfoPage.url)
    })
})
