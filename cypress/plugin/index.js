const XlsxPopulate = require('xlsx-populate');
const XLSX = require('xlsx');
const {downloadFile} = require('cypress-downloadfile/lib/addPlugin');
var pdfParser = require('pdf-parser');
const fs = require('fs');
const pdf = require('pdf-parse');
module.exports = (on, config) => {

  on('task', {

    async modifyExcelFile({filePath, sheetName, cell, value}) {

      try {

        const workbook = await XlsxPopulate.fromFileAsync(filePath);
        const sheet = workbook.sheet(sheetName);
        // Write the cell value
        sheet.cell(cell).value(value);

        // Save the modified workbook
        await workbook.toFileAsync(filePath);
        return null;

      } catch (error) {
        throw new Error(`Error modifying Excel file: ${error}`);

      }

    },

  });

  on('task', {

    async readExcelFile({filePath, sheetName}) {

      try {

        const workbook = XLSX.read(filePath, { type: 'binary' });
        const worksheet = workbook.Sheets[sheetName];
        const data = XLSX.utils.sheet_to_json(worksheet);
        return data

      } catch (error) {
        throw new Error(`Error modifying Excel file: ${error}`);

      }

    },
  });
 // on('task', {downloadFile});



}