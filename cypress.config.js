const { defineConfig } = require("cypress");
const cred = require('./cypress/fixtures/credentials.json')

module.exports = defineConfig({
  e2e: {
    
    setupNodeEvents(on, config) {
    
  },
  baseUrl: cred.application.cogitate.baseurl,
  specPattern:"cypress\\e2e\\commonTestFile",
  env:{
    username:cred.application.cogitate.engs.username,
    password:cred.application.cogitate.engs.password,
    endpoint:cred.application.cogitate.engs.endpoint
 },
  }
});
