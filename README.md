
# Project Title - Cogitate-Toga-Engs-Poc

This is proof of concept of cogitate Engs and Toga insurance product



## Installation

To install the project, follow the steps below

```bash
  1. Open the terminal 
  2. Install dependencies: npm i
    ## Running Tests

to run toga test files in headed mode

```bash
npx cypress run  --config-file ./toga.config.js --browser chrome --headed --spec 'cypress/e2e/togaTestFile/*.cy.js'
```

to run toga test files in headless mode

```bash
npx cypress run --config-file ./toga.config.js --browser chrome --spec 'cypress/e2e/togaTestFile/*.cy.js'
```

to run engs test files in headed mode

```bash
npx cypress run --config-file ./engs.config.js --browser chrome --headed --spec 'cypress\\e2e\\engsTestFile\\*.cy.js'
```

to run engs test files in headless mode

```bash
npx cypress run --config-file ./engs.config.js --browser chrome  --spec 'cypress\\e2e\\engsTestFile\\*.cy.js'
```
to open the engs test runner files
```bash
npx cypress open  --config-file ./engs.config.js
```
to open the toga test runner files
```bash
npx cypress open  --config-file ./toga.config.js
```
to run common file headed mode
```bash
npx cypress run --headed --spec 'cypress\\e2e\\commonTestFile\\*.cy.js'
```
to run common file headless mode
```bash
npx cypress run --spec 'cypress\\e2e\\commonTestFile\\*.cy.js'
```


## Folder Structure

```bash
cypress-project/
  ├── cypress/
  │   ├── fixtures/
  │   │   └── example.json
  │   ├── e2e/
  |   |     └── common test files
  |   |     |      └── example.cy.js
  |   |     └── toga test files
  |   |     |      └── example.cy.js
  |   |     └── engs test files
  |   |     |         └── example.cy.js
  |   |     └── page objects
  │   │           └── loginPage
  |   |                     └── example.js
  │   └── support/     
  │       ├── commands.js
  │       └── e2e.js
  ├── node_modules/
  ├── .gitignore
  ├── cypress.config.json
  ├── package-lock.json
  └── package.json
```

  The cypress directory contains all the test files and configuration for Cypress. Here's what each directory contains:


fixtures: contains data used by tests, such as JSON or CSV files.

e2e: contains the actual tests written in javaScript files.

support: contains javaScript files that can be used to write custom commands or hooks.

## Cypress Configuration

The cypress.config.json file contains configuration options for cypress. You can set various options such as the base URL for the application, the viewport size, and much more.
## Writing Tests

Tests are written using cypress's built-in testing framework. To write a test, create a new file in the cypress/e2e directory and use the describe and it functions to define your test suites and test cases

```bash
describe('Example test suite', () => {
  it('Example test case', () => {
    // Test code goes here
  })
})
```

You can use various cypress commands such as cy.visit(), cy.get(), cy.click(), and many more to interact with the application and perform actions.