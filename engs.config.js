const { defineConfig } = require("cypress");
const { downloadFile } = require('cypress-downloadfile/lib/addPlugin');
const fs = require('fs');
const pdf = require('pdf-parse');
var pdfParser = require('pdf-parser');
const cred = require('./cypress/fixtures/credentials.json')
module.exports = defineConfig({
  projectId: "1izrrv",
  e2e: {
     
    setupNodeEvents(on, config) {
        // implement node event listeners here
        on('task', { downloadFile })
        on('task', {
          async readPdf(filePath) {
            let dataBuffer = fs.readFileSync(filePath);
            const data = pdf(dataBuffer)
            return data
          }
        })
        //read file
        on('task', {
          getPdf(filePath) {
            return new Promise((resolve, reject) => {
              pdfParser.pdf2json(filePath, function (error, pdf) {
                if (error) {
                  reject(error);
                } else {
                  const data = pdf;
                  resolve(data);
                }
              });
            });
  
            // return require('./cypress/plugin/index')(on, config)
  
          },
        })
        //delete the file
        on('task', {
          deleteFile(directory) {
            try {
              fs.readdirSync(directory).forEach((file) => {
                fs.unlinkSync(`${directory}/${file}`);
              });
              return null;
            } catch (err) {
              // Return the error message if the file deletion fails
              return err.message;
            }
          },
        });
      },
      env:{
         username:cred.application.cogitate.engs.username,
         password:cred.application.cogitate.engs.password,
         endpoint:cred.application.cogitate.engs.endpoint
      },
    specPattern: 'cypress\\e2e\\engsTestFile\\*.cy.js',
    supportFile: 'cypress/support/e2e.js',
    baseUrl: cred.application.cogitate.baseurl,
    defaultCommandTimeout: 100000,
    screenshotOnRunFailure: true,
    video: false,
    retries:2
  },
});
